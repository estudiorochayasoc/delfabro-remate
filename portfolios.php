<?php
require_once "Config/Autoload.php";
require_once "vendor/autoload.php";

use JasonGrimes\Paginator;

Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$portfolios = new Clases\Portfolio();
$categoria = new Clases\Categorias();

#Variables GET
$categoriaGET = isset($_GET["categoria"]) ? $f->antihack_mysqli($_GET["categoria"]) : null;
$subcategoriaGET = isset($_GET["subcategoria"]) ? $f->antihack_mysqli($_GET["subcategoria"]) : null;
$buscarGET = isset($_GET["buscar"]) ? $f->antihack_mysqli($_GET["buscar"]) : null;

#List de portfolios
$portfoliosArray = $portfolios->list("", "", "");
#List de últimos portfolios
$portfoliosArraySide = $portfolios->list("", "", 8);
#List de categorias
$categoriasArray = $categoria->list(["area = 'portfolio'"], "", "");

#Se filtra el array de portfolios por categoria, subcategoria y busqueda
$portfoliosArray = $portfolios->filterArray($categoriaGET, $subcategoriaGET, $buscarGET, $portfoliosArray);

#Opciones del paginador
$itemsPerPage = 10;
$totalItems = count($portfoliosArray);
$currentPage = isset($_GET["pagina"]) ? $f->antihack_mysqli($_GET["pagina"]) : 1;
$urlPattern = preg_replace("/\/pagina\/(\d+)/i", "", CANONICAL) . '/pagina/(:num)';
$urlPattern = str_replace("portfolios//", "portfolios/", $urlPattern);

$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
$paginator->setMaxPagesToShow(10);
$paginator->setPreviousText("Anterior");
$paginator->setNextText("Siguiente");

#Con las opciones del paginador se limita la cantidad de portfolios a mostrar en cada página
$portfoliosArray = array_slice($portfoliosArray, (($currentPage - 1) * $itemsPerPage), $itemsPerPage);

#Información de cabecera
$template->set("title", "Portfolio | " . TITULO);
$template->set("description", "Mirá todos nuestros trabajos.");
// $template->set("keywords", "portfolios bremocor, portfolios motores electricos san francisco, actualidad motores electricos, noticias motores electricos");
$template->themeInit();
?>
<!-- start page-title -->
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <h2>Portfolio</h2>
                <ol class="breadcrumb">
                    <li><a href="<?= URL ?>">Inicio</a></li>
                    <li>Portfolio</li>
                </ol>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>
<!-- end page-title -->

<!-- start blog-with-sidebar -->
<section class="blog-with-sidebar section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 col-md-8">
                <div class="news-grids">
                    <?php
                    foreach ($portfoliosArray as $portfolioItem) {
                        $img = isset($portfolioItem['images'][0]['ruta']) ? $portfolioItem['images'][0]['ruta'] : 'assets/archivos/sin_imagen.jpg';
                        $img = URL . '/' . $img;
                        $fecha = strftime("%u de %B de %Y", strtotime($portfolioItem['data']['fecha']));
                        $link = URL . '/portfolio/' . $f->normalizar_link($portfolioItem['data']["titulo"]) . '/' . $portfolioItem['data']['cod'];
                    ?>
                        <div class="grid">
                            <a href="<?= $link ?>">
                                <div class="entry-media" style="background: url(<?= $img ?>)center/cover no-repeat; height: 300px;"></div>
                            </a>
                            <div class="entry-details">
                                <div class="entry-meta">
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i><?= $fecha ?></li>
                                    </ul>
                                </div>
                                <div class="entry-body">
                                    <h3 class="title-blog-overflow"><a href="<?= $link ?>"><?= mb_substr($portfolioItem["data"]["titulo"], 0, 60) ?></a></h3>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div> <!-- end news-grids -->

                <div class="pagination-wrapper">
                    <div class="col-md-12 text-center"><?= $paginator; ?></div>
                </div>
            </div> <!-- end blog-content -->

            <div class="blog-sidebar col col-lg-3 col-lg-offset-1 col-md-4 col-sm-5">
                <div class="widget search-widget">
                    <form class="form" method="get" action="<?= URL ?>/portfolios">
                        <input oninvalid="this.setCustomValidity('Únicamente se permiten letras y números.')" oninput="this.setCustomValidity('')" type="text" name="buscar" pattern="[^()/><\][\\\x22#'´`¨{}^*,;.|!¡?¿$%&]+" class="form-control" placeholder="Buscar..">
                    </form>
                </div>
                <div class="widget category-widget">
                    <h3>Categorías</h3>
                    <ul>
                        <?php foreach ($categoriasArray as $categoriaItem) { ?>
                            <li>
                                <a href="<?= URL . '/portfolios/' . $f->normalizar_link($categoriaItem["data"]["titulo"]) ?>">
                                    <?= $categoriaItem["data"]["titulo"] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="widget recent-post-widget">
                    <h3>Reciente</h3>
                    <ul>
                        <?php
                        foreach ($portfoliosArraySide as $portfolioItem) {
                            $img = isset($portfolioItem['images'][0]['ruta']) ? $portfolioItem['images'][0]['ruta'] : 'assets/archivos/sin_imagen.jpg';
                            $img = URL . '/' . $img;
                            $fecha = strftime("%u de %B de %Y", strtotime($portfolioItem['data']['fecha']));
                            $link = URL . '/portfolio/' . $f->normalizar_link($portfolioItem['data']["titulo"]) . '/' . $portfolioItem['data']['cod'];
                        ?>
                            <li>
                                <div class="post-pic" style="background: url(<?= $img ?>)center/cover no-repeat; height: 67px;"></div>
                                <div class="details">
                                    <span><?= $fecha ?></span>
                                    <h4 class="title-blog-side-overflow"><a href="<?= $link ?>"><?= mb_substr($portfolioItem['data']['titulo'], 0, 60); ?></a></h4>
                                </div>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- end container -->
</section>
<!-- end blog-with-section -->

<?php $template->themeEnd() ?>