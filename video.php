<?php
require_once "Config/Autoload.php";
Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$videos = new Clases\Videos();
$categoria = new Clases\Categorias();

$cod = $f->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');

#View de la novedad actual
$videos->set("cod", $cod);
$videoData = $videos->view();

#Redireccionar si no existe la novedad actual
empty($videoData['data']) ? $f->headerMove(URL . '/videos') : null;

#List de últimos videos
$videosArraySide = $videos->list("", "", 8);

#Información de cabecera
$template->set("title", ucfirst(mb_strtolower($videoData['data']['titulo'])) . " | " . TITULO);
$template->set("description", substr(strip_tags($videoData['data']['descripcion'], '<p><a>'), 0, 160));
$template->set("keywords", "");
$template->themeInit();
?>

<!-- start blog-with-sidebar -->
<section class="blog-single section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12 blog-single-content">
                <div class="post">
                    <div class="media">
                        <iframe width="100%" height="650" src="https://www.youtube.com/embed/<?= substr($videoData['data']['link'], -11) ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="post-title-meta">
                        <h2><?= $videoData['data']['titulo'] ?></h2>
                    </div>
                    <div class="post-body">
                        <?= $videoData['data']['descripcion'] ?>
                    </div>
                </div> <!-- end post -->

                <div class="tag-share">
                    <div>
                        <span></span>
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_email"></a>
                            <a class="a2a_button_google_gmail"></a>
                            <a class="a2a_button_whatsapp"></a>
                        </div>
                        <script async src="https://static.addtoany.com/menu/page.js"></script>
                    </div>
                </div>

                <div>
                    <div class="fb-comments" data-href="<?= URL . '/video/' . $f->normalizar_link($videoData['data']['titulo']) . '/' . $videoData['data']['cod'] ?>" data-width="100%" data-numposts="5" data-order-by="social"></div>
                </div>
            </div> <!-- end blog-content -->
        </div>
    </div> <!-- end container -->
</section>
<!-- end blog-with-section -->

<?php $template->themeEnd() ?>