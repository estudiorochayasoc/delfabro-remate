<?php
require_once "Config/Autoload.php";
Config\Autoload::run();
$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$producto = new Clases\Productos();
$usuarios = new Clases\Usuarios();
$ofertas = new Clases\Ofertas();
$contenidos = new Clases\Contenidos();
$config = new Clases\Config();
$contactData = $config->viewContact();

#Variables GET
$cod = $f->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');

#Se carga la sesión del usuario
$usuario = $usuarios->viewSession();

#Si existe la sesión y no es invitado, entonces se habilita el botón de mi cuenta en la nav
$habilitado = (isset($usuario["invitado"]) && $usuario["invitado"] == 0) ? true : false;

#View del producto actual
$producto->set("cod", $cod);
$productoData = $producto->view();

// CONTENIDO CONDICIONES COMERCIALES
$contenidos->set("cod", "c47fc8e394");
$contenidoCondiciones = $contenidos->view();



#Se redirecciona si el producto no existe
empty($productoData) ? $f->headerMove(URL . '/productos') : null;

#Información de cabecera
$template->set("title", ucfirst(mb_strtolower($productoData['data']['titulo'])) . " | " . TITULO);
$template->set("description", mb_substr(strip_tags($productoData['data']['desarrollo'], '<p><a>'), 0, 160));
$template->set("keywords", strip_tags($productoData['data']['keywords']));
$template->set("imagen", isset($productoData['images'][0]['ruta']) ? URL . '/' . $productoData['images'][0]['ruta'] : LOGO);
$template->themeInit();

?>

<div class="single-product-section mt-50 section pt-10 pb-50 pb-lg-80 pb-md-70 pb-sm-30 pb-xs-20">
    <div class="container">
        <div class="product-view  shop-area">
            <div class="row">
                <div class="content-product-left class-honizol col-md-6 col-sm-12 col-xs-12 mb-20">
                    <?php
                    if (!empty($productoData['data']['variable1'])) {
                    ?>
                        <div class="product-description mb-20">
                            <iframe width="100%" height="355" src="https://www.youtube.com/embed/<?= $productoData['data']['variable1'] ?>?rel=0" allow=";" allowfullscreen="" frameborder="0"></iframe>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <button type="button" class="btn btn-warning pull-right" style="margin-top: 3px; margin-right: 3px;" data-toggle="modal" data-target="#condiciones"><i class="fa fa-gavel" aria-hidden="true"></i> VER CONDICIONES</button>
                        <div class="panel-heading">

                            <?php if (!empty($productoData['category'])) { ?>
                                <div class="text-uppercase">
                                    <a href="<?= URL ?>/productos/c/<?= $f->normalizar_link($productoData['category']['data']['titulo']) ?>/<?= $productoData['category']['data']['id'] ?>">
                                        <?= $productoData['category']['data']['titulo'] ?>
                                    </a>
                                    <?php if (!empty($productoData['subcategory']['data'])) { ?>
                                        <a href="<?= URL ?>/productos/s/<?= $f->normalizar_link($productoData['category']['data']['titulo']) ?>/<?= $productoData['category']['data']['id'] ?>/<?= $f->normalizar_link($productoData['subcategory']['data']['titulo']) ?>/<?= $productoData['subcategory']['data']['id'] ?>">
                                            <?= " / " . $productoData['subcategory']['data']['titulo'] ?>
                                        </a>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="panel-body">
                            <h3 class="text-uppercase"><?= $productoData['data']['titulo'] ?></h3>
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <?php
                                    $a =  0;
                                    for ($i = 7; $i >= 2; $i--) {
                                        if (!empty($productoData['data']["variable$i"])) {          
                                    ?>
                                            <li role="presentation" class="<?= ($a == 0) ? 'active' : '' ?>"><a href="#RP<?= $i ?>" aria-controls="home" role="tab" data-toggle="tab"><?= ($i - 1) . " - " . $productoData['data']['variable' . "$i"] ?></a></li>
                                        <?php
                                         $a++;
                                        }
                                        ?>
                                    <?php
                                    }
                                    ?>
                                </ul>
                                <div class="tab-content mt-15">
                                    <?php
                                    $rpVar = [];
                                    $b = 0;
                                    for ($i = 7; $i >= 2; $i--) {
                                        if (!empty($productoData['data']["variable$i"])) {
                                            
                                            $rpVar[] = $productoData['data']["variable$i"];
                                            $ofertasProducto = $ofertas->list(["rp = '" . $productoData['data']["variable$i"] . "'", "producto = '" . $productoData['data']["cod"] . "'"], "", "");
                                            $precioOfertaProducto = !empty($ofertasProducto) ? $ofertasProducto[0]["oferta"] : $productoData['data']["precio"];
                                            $precio = $precioOfertaProducto;
                                            $rp = !empty($ofertasProducto) ? $ofertasProducto[0]["rp"] : '';

                                            $lotes2500 = array('49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '70');
                                            if ($ofertas->strposa($productoData['data']['titulo'], $lotes2500, 1)) {
                                                $sumar = 2500;
                                            } else {
                                                $sumar = 5000;
                                            }

                                            if (!empty($usuario)) {
                                                $ofertaData = $ofertas->list(["usuario = '" . $usuario["cod"] . "'", "rp = '" . $productoData['data']["variable$i"] . "'",  "producto = '" .  $productoData["data"]["cod"] . "'"], "", "");
                                                $precioOfertaUsuario = !empty($ofertaData) ? $ofertaData[0]["oferta"] : '1';
                                            }
                                    ?>
                                            <div role="tabpanel" class="tab-pane fade <?= ($b == 0) ? 'in active' : '' ?>" id="RP<?= $i ?>">
                                                <?php
                                                $b++;
                                                if ($precioOfertaProducto != $productoData['data']['precio']) {
                                                ?>
                                                    <div class="fs-31 mt-15">
                                                        <span class="fs-35" id="refreshPrice<?= $f->normalizar_link($productoData['data']["variable$i"]) ?>"> </span>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="fs-30 mt-20 mb-10">
                                                        <span class="fs-35">$<?= number_format($productoData['data']['precio'], 0, ",", ".") ?> </span>
                                                    </div>
                                                    <?php }

                                                // ------- BANDERA FINALIZO REMATE DEL PRODUCTO------------------------------------------
                                                if ($productoData['data']['variable8'] != 1) {
                                                    //------ OFERTA REMATE --------------------------------------------------------------------------
                                                    if (!$habilitado) { ?>
                                                        <a href="<?= URL ?>/usuarios?getReturnURL=<?= CANONICAL ?>">
                                                            <button class="btn btn-warning mb-44 mt-23 fs-20 bold ">
                                                                <i class="fa fa-user"></i> Iniciar Sesión
                                                            </button>
                                                        </a>
                                                    <?php } elseif ($precioOfertaProducto == $precioOfertaUsuario) { ?>
                                                        <a href="<?= URL ?>/sesion/pedidos">
                                                            <button class="btn btn-warning mt-25 mb-30 fs-20 bold ">
                                                                <i class="fa fa-user"></i> Mis Ofertas
                                                            </button>
                                                        </a>
                                                        <?php } else {
                                                        if (new DateTime() < new DateTime("2020-08-31 00:00:00")) {
                                                            echo "<div class='alert alert-warning'>El sistema de pre-ofertas será habilitado el día 31 de Agosto a las 00:00hs.<br/> Muchas gracias</div>";
                                                            echo "<a class='bold btn btn-success mb-10 btn-block' href='https://api.whatsapp.com/send?phone=549" . $contactData['data']['whatsapp'] . "' target='_blank'><i class='fa fa-whatsapp'></i> CONSULTAR POR WHATSAPP</a>";
                                                        } else {
                                                        ?>
                                                            <form class="row pl-15 pr-15" id="<?= $f->normalizar_link($productoData['data']["variable$i"]) ?>" onsubmit="sendForm('<?= $f->normalizar_link($productoData['data']['variable' . $i]) ?>')">
                                                                <label for="ofertaList">Ofertar:</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">$</span>
                                                                    <select class="form-control" name="oferta" id="ofertaList">
                                                                        <?php
                                                                        for ($o = 0; $o < 8; $o++) {
                                                                            $precio += $sumar;
                                                                            echo '<option>' . $precio . '</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <input type="hidden" name="rp" value="<?= $productoData['data']["variable$i"] ?>">
                                                                <input type="hidden" name="producto" value="<?= $productoData['data']["cod"] ?>">
                                                                <input type="hidden" name="usuario" value="<?= $usuario['cod'] ?>">
                                                                <input type="hidden" name="estado" value="<?= $usuario['estado'] ?>">
                                                                <input type="hidden" name="agregar" value="1" />
                                                                <input type="submit" class="btn btn-block btn-warning fs-20 bold mt-15" value="OFERTAR" />
                                                            </form>
                                                    <?php
                                                        }
                                                    } ?>
                                                    <div class="panel-footer pl-15 pr-15">
                                                        <?php
                                                        if ($precioOfertaProducto != $productoData['data']['precio']) { ?>
                                                            <h3 class="fs-16 text-uppercase">Historial de Ofertas</h3>
                                                            <div class="card card-body product_bar">
                                                                <table class="table table-hover">
                                                                    <thead>
                                                                        <th>FECHA</th>
                                                                        <th>OFERTA</th>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        foreach ($ofertasProducto as $historial) {
                                                                            if ($historial["estado"] != 0) {
                                                                                $fecha = date_create($historial["fecha"]);
                                                                        ?>
                                                                                <tr>
                                                                                    <td><?= date_format($fecha, "d/m/Y h:m") ?></td>
                                                                                    <td>$ <?= $historial['oferta'] ?></td>
                                                                                </tr>
                                                                        <?php }
                                                                        }    ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        <?php
                                                        }  ?>
                                                    </div>
                                                <?php } else { ?>
                                                    <span class="fs-18 text-uppercase label label-success">Pre-Oferta Finalizada</span>
                                                    <h2 class="mt-20 text-uppercase fs-16">
                                                        Podrás ofertar el día 10 de Septiembre sera el remate en vivo en este sitio web
                                                    </h2>
                                                <?php } ?>
                                            </div>

                                        <?php
                                        }
                                        ?>
                                    <?php
                                    
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                if (!strpos($productoData['images'][0]["ruta"], "sin_imagen")) {
                ?>

                    <div class="col-md-6">
                        <h2 class="section-title text-uppercase fs-22 pb-0 mb-0">
                            Fotos
                            <hr class="mt-0 pt-0" />
                        </h2>
                        <div id="carouselExampleControls" class="carousel slide mb-30" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php foreach ($productoData['images'] as $key => $img) { ?>
                                    <div class="item <?= ($key == 0) ? "active" : null ?>">
                                        <img src="<?= URL ?>/<?= $img['ruta'] ?>" width="100%" alt="">
                                    </div>
                                <?php } ?>
                            </div>
                            <?php if (count($productoData['images']) > 1) { ?>
                                <a class="left carousel-control" href="#carouselExampleControls" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                    <span class="sr-only">Anterior</span>
                                </a>
                                <a class="right carousel-control" href="#carouselExampleControls" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    <span class="sr-only">Siguiente</span>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                    <?php } else { ?>
                        <div class="col-md-12">
                        <?php } ?>
                        <h2 class="section-title text-uppercase fs-22 pb-0 mb-0">
                            FICHA ANIMAL
                            <hr class="mt-0 pt-0" />
                        </h2>
                        <p class="fs-16"><?= $productoData['data']['desarrollo']  ?></p>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="condiciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title text-uppercase" id="exampleModalLabel"><?= $contenidoCondiciones['data']['titulo'] ?></h3>
                </div>
                <div class="modal-body">
                    <p><?= $contenidoCondiciones['data']['contenido'] ?></p>
                </div>
            </div>
        </div>
    </div>



    <?php $template->themeEnd(); ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?= URL ?>/assets/js/services/products.js"></script>
    <script src="<?= URL ?>/assets/js/services/offer.js"></script>
    <script>
        setInterval(() => {
            refreshPrice('<?= URL ?>', '<?= $cod ?>', '<?= implode(",", $rpVar) ?>')
        }, 1000);

        function sendForm(form) {
            //consulta ajax
            event.preventDefault();

            swal({
                    title: "¿Seguro desea realizar la oferta?",
                    text: "",
                    icon: "warning",
                    closeOnClickOutside: false,
                    buttons: ["No", "Si, quiero ofertar!"],
                })
                .then((d) => {
                    if (d) {
                        $.ajax({
                            url: "<?= URL ?>/api/offer/add.php",
                            type: "POST",
                            data: $("#" + form).serialize(),
                            success: function(data) {
                                data = JSON.parse(data);
                                //console.log(data);
                                if (data.result) {
                                    swal({
                                            title: "Felicitaciones su Pre-Oferta fue realizada con éxito.",
                                            icon: "success",
                                            closeOnClickOutside: false,
                                            button: "Ok",
                                        })
                                        .then((ex) => {
                                            if (ex) {
                                                location.reload();
                                            }
                                        });
                                } else {
                                    swal({
                                            title: "Ups! Alguien supero su oferta hace unos momentos.",
                                            icon: "error",
                                            closeOnClickOutside: false,
                                            button: "Ok",
                                        })
                                        .then((er) => {
                                            if (er) {
                                                location.reload();
                                            }
                                        })
                                }

                            }
                        });
                    }
                });
        }
    </script>