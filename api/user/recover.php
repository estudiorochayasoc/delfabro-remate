<?php
require_once dirname(__DIR__, 2) . "/Config/Autoload.php";
Config\Autoload::run();
$f = new Clases\PublicFunction();
$usuario = new Clases\Usuarios();
$config = new Clases\Config();
$captchaData = $config->viewCaptcha();

// Verify the reCAPTCHA response
$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $captchaData['data']['captcha_secret'] . '&response=' . $_POST['g-recaptcha-response']);
$responseData = json_decode($verifyResponse);
if ($responseData->success) {
    $email = $f->antihack_mysqli(isset($_POST['email']) ? $_POST['email'] : '');
    if (!empty($email)) {
        $usuario->set("email", $email);
        $response = $usuario->validate();
        if ($response['status']) {
            echo json_encode(["status" => true]);
        } else {
            echo json_encode(["status" => false, "type" => "error", "message" => "No existe un usuario registrado con ese correo"]);
        }
    } else {
        echo json_encode(["status" => false, "type" => "error", "message" => "Completar el campo correctamente."]);
    }
} else {
    echo json_encode(["status" => false, "type" => "error", "message" => "¡Completar el CAPTCHA correctamente!"]);
}
