<?php
require_once dirname(__DIR__, 2) . "/Config/Autoload.php";
Config\Autoload::run();
$f = new Clases\PublicFunction();
$usuario = new Clases\Usuarios();
$config = new Clases\Config();
$checkout = new Clases\Checkout();
$captchaData = $config->viewCaptcha();

// Verify the reCAPTCHA response
$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $captchaData['data']['captcha_secret'] . '&response=' . $_POST['g-recaptcha-response']);
$responseData = json_decode($verifyResponse);
if ($responseData->success) {
    $user = $f->antihack_mysqli(isset($_POST['l-user']) ? $_POST['l-user'] : '');
    $pass = $f->antihack_mysqli(isset($_POST['l-pass']) ? $_POST['l-pass'] : '');
    $stage = $f->antihack_mysqli(isset($_POST['stg-l']) ? $_POST['stg-l'] : '');
    if (!empty($user) && !empty($pass)) {
        $usuario->set("email", $user);
        $usuario->set("password", $pass);
        $response = $usuario->login();
        if (isset($response['error'])) {
            if ($response['error'] == 1) {
                echo json_encode(["status" => false, "type" => "error", "message" => "El usuario no esta activado, comunicarse con el soporte."]);
            } else {
                echo json_encode(["status" => false, "type" => "error", "message" => "Email o contraseña incorrectos."]);
            }
        } else {
            if (!empty($stage)) {
                $checkout->user($_SESSION['usuarios']['cod'], 'USER');
            }
            echo json_encode(["status" => true]);
        }
    } else {
        echo json_encode(["status" => false, "type" => "error", "message" => "Completar ambos campos."]);
    }
} else {
    echo json_encode(["status" => false, "type" => "error", "message" => "¡Completar el CAPTCHA correctamente!"]);
}
