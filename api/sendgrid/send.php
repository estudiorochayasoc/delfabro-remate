<?php

require dirname(__DIR__, 2) . '/vendor/autoload.php';

$to = isset($_GET["email"]) ? $_GET["email"] : '';
$subject = isset($_GET["subject"]) ? $_GET["subject"] : '';
$body = isset($_GET["body"]) ? $_GET["body"] : '';

$email = new \SendGrid\Mail\Mail();
$email->setFrom("delfabro@delfabro.com.ar", "Delfabro Agropecuaria");
$email->setSubject($subject);
$email->addTo($to, "");
$email->addContent("text/plain", strip_tags($body));
$email->addContent("text/html", $body);

$sendgrid = new \SendGrid('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');
try {
    $response = $sendgrid->send($email);
    print $response->statusCode() . "\n";
    print_r($response->headers());
    print $response->body() . "\n";
} catch (Exception $e) {
    echo 'Caught exception: ' . $e->getMessage() . "\n";
}
