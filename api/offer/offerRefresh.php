<?php
require_once dirname(__DIR__, 2) . "/Config/Autoload.php";
Config\Autoload::run();
$f = new Clases\PublicFunction();
$ofertas = new Clases\Ofertas();
$producto = new Clases\Productos();

#Variables GET
$cod = isset($_GET["cod"]) ? $f->antihack_mysqli($_GET["cod"]) : '';
$rp = isset($_GET["rp"]) ? $f->antihack_mysqli($_GET["rp"]) : '';
$rpExplode = explode(",", $rp);
// $producto->set("cod", $cod);
// $productoData = $producto->view();

$arrayResponse = [];
#List del producto actual
foreach ($rpExplode as $rp_) {
    $ofertasProducto__ = $ofertas->list(["rp = '" . $rp_ . "'", "producto = '" . $_GET["cod"] . "'", "estado =  '1'"], "", "");
    $price = !empty($ofertasProducto__) ? $ofertasProducto__[0]["oferta"] : 0;
    $arrayResponse[] = ["rp" => $f->normalizar_link($rp_), "price" => $price];
}

echo json_encode($arrayResponse);
