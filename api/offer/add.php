<?php
require_once dirname(__DIR__, 2) . "/Config/Autoload.php";
Config\Autoload::run();
$f = new Clases\PublicFunction();
$producto = new Clases\Productos();
$ofertas = new Clases\Ofertas();
$usuarios = new Clases\Usuarios();
$enviar = new Clases\Email();
$config = new Clases\Config();

$emailData = $config->viewEmail();

$producto->set("cod", $_POST["producto"]);
$productoData = $producto->view();
$link = URL . '/producto/' . $f->normalizar_link($productoData["data"]["titulo"]) . '/' . $productoData["data"]["cod"];

$ofertasProducto_ = $ofertas->list(["rp = '" . $_POST["rp"] . "'", "producto = '" . $_POST["producto"] . "'"], "", "1");
$precioOfertaProducto_ = !empty($ofertasProducto_) ? $ofertasProducto_[0]["oferta"] : $productoData['data']['precio'];

$lotes2500 = array('49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70');


if ($ofertas->strposa($productoData['data']['titulo'], $lotes2500)) {
    $sumar_ = 2500;
}else{
    $sumar_ = 5000;
}


$precioA_ = $precioOfertaProducto_;
$result = false;

// FORM REALIZAR OFERTA
if (isset($_POST["agregar"])) {
    $textoPrecio = '';
    for ($i = 0; $i < 8; $i++) {
        $precioA_ += $sumar_;
        $textoPrecio .= "|" . $precioA_;
    }
    if (strpos($textoPrecio, $_POST["oferta"])) {
        if ($_POST["oferta"] > $precioOfertaProducto_) {
            $cod = substr(md5(uniqid(rand())), 0, 10);
            $ofertas->set("cod", $cod);
            $ofertas->set("oferta", isset($_POST["oferta"]) ? $f->antihack_mysqli($_POST["oferta"]) : '');
            $ofertas->set("rp", isset($_POST["rp"]) ? $f->antihack_mysqli($_POST["rp"]) : '');
            $ofertas->set("producto", $_POST["producto"]);
            $ofertas->set("usuario", $_POST["usuario"]);
            $ofertas->estado = isset($_POST["estado"]) ? $f->antihack_mysqli($_POST["estado"]) : 0;
            $ofertas->set("fecha", date("Y-m-d-H-i-s"));

            if ($ofertas->add()) {
                $result = true;
                if (!empty($ofertasProducto_)) {
                    $usuarios->set("cod", $ofertasProducto_[0]['usuario']);
                    $usuarioData = $usuarios->view();
                    if ($_POST['usuario'] != $ofertasProducto_[0]['usuario']) {
                        ($productoData['data']['titulo'] != $_POST['rp']) ?  $rpEmail = " - RP: " . $_POST['rp']  : $rpEmail = '';


                        //Envio de mail al usuario
                        $mensaje = "
                        <img src='" . LOGO . "' width='300px'/>
                        <hr style='margin:10px 0px;'/>
                        <p>Estimado " . $usuarioData['data']['nombre'] . " " . $usuarioData['data']['apellido'] . ",</p>
                        <p>En el " . $productoData['data']['titulo'] . $rpEmail . " realizaron una oferta de <b>$" . $_POST['oferta'] . "</b> la cual supera a su oferta realizada.</p>
                        <p>Para superar la oferta haga <a href='$link'><u>click aquí</u></a> y realice una nueva oferta.</p>
                        <p>Atte. Delfabro Agropecuaria</p>
                        ";
                        $asunto = "Superaron tu pre-oferta en el " . $productoData['data']['titulo'] . " - Delfabro Agropecuaria";
                        $enviar->set("asunto", $asunto);
                        $enviar->set("receptor", $usuarioData['data']['email']);
                        $enviar->set("emisor", $emailData['data']['remitente']);
                        $enviar->set("mensaje", $mensaje);
                        $enviar->emailEnviarCurl();
                    }
                }
            }
        }
    }
}


echo json_encode(["result" => $result]);
