<?php
require_once dirname(__DIR__, 2) . "/Config/Autoload.php";
Config\Autoload::run();
$funciones = new Clases\PublicFunction();
$producto = new Clases\Productos();
$categoria = new Clases\Categorias();
$subcategoria = new Clases\Subcategorias();
$ofertas = new Clases\Ofertas();

if (empty($_POST["title"])) {
    unset($_POST["title"]);
}

$title = $funciones->antihack_mysqli(isset($_POST['title']) ? $_POST['title'] : '');
$order = $funciones->antihack_mysqli(isset($_GET['order']) ? $_GET['order'] : '');
$start = $funciones->antihack_mysqli(isset($_GET['start']) ? $_GET['start'] : '0');
$limit = $funciones->antihack_mysqli(isset($_GET['limit']) ? $_GET['limit'] : '24');


if (count($_POST) < 1) {
    $filter = '';
} else {
    $filter = [];
}

if (!empty($title)) {
    $filter[] = "titulo LIKE '%$title%' OR cod_producto = '$title'";
}


if (!empty($_POST['categories'])) {
    $categoryFilter = '';
    foreach ($_POST['categories'] as $key => $cat) {
        $cat_ = $funciones->antihack_mysqli($cat);
        $key == count($_POST['categories']) - 1 ? $or = '' : $or = ' || ';
        $categoryFilter .= !empty($cat_) ? "categoria='" . $cat_ . "'" . $or : '';
    }
    $filter[] = $categoryFilter;
}

if (!empty($_POST['subcategories'])) {
    $subcategoryFilter = '';
    foreach ($_POST['subcategories'] as $key => $cat) {
        $subcat_ = $funciones->antihack_mysqli($cat);

        $key == count($_POST['subcategories']) - 1 ? $or = '' : $or = ' || ';
        //echo $key;
        $subcategoryFilter .= !empty($subcat_) ? "subcategoria='" . $subcat_ . "'" . $or : '';
        //$subcategoryFilter .= !empty($cat_) ? "variable1='" . $cat_ . "'" . $or : '';
    }
    $filter[] = $subcategoryFilter;
}


switch ($order) {
    default:
        $order = "id ASC";
        break;
    case "2":
        $order = "precio ASC";
        break;
    case "3":
        $order = "precio DESC";
        break;
}

if (empty($filter)) $filter = '';
$productosData = $producto->list($filter, $order, $start . "," . $limit);

if (!empty($productosData)) {
    foreach ($productosData as $key => $productoItem) {
        $listOffer = [];
        if ($productoItem['data']['variable9'] == 1) {
            $link = URL . '/producto/' . $funciones->normalizar_link($productoItem["data"]["titulo"]) . '/' . $productoItem["data"]["cod"];

            for ($i = 2; $i <= 7; $i++) {
                if (!empty($productoItem['data']["variable$i"])) {
                    $ofertasProducto = $ofertas->list(["rp = '" . $productoItem['data']["variable$i"] . "'", "producto = '" . $productoItem['data']["cod"] . "'"], "", "");
                    $listOffer[] = ["rp" => $productoItem['data']["variable$i"], "price" => (isset($ofertasProducto[0]['oferta']) ?  $ofertasProducto[0]['oferta'] : $productoItem['data']['precio'])];
                }
            }
?>
            <div onclick="window.location.assign('<?= $link ?>')">
                <div class="col-md-4 col-xs-12 mb-20">
                    <div class="grid mb-10 height-p">
                        <div class="img-holder-info-list">
                            <div class="img-holder height-170 " style="background: url('<?= URL . "/" . $productoItem["images"][0]["ruta"] ?>') center center/contain no-repeat;"></div>
                        </div>
                        <div class="product-info  height-145" style="margin-top: 4px;">
                            <h3 class="fs-18 bold"><a href="<?= $link ?>"><?= $productoItem["data"]["titulo"] ?></a></h3>
                            <?php
                            if ($productoItem['data']['variable8'] == 1) {
                            ?>
                                <span class='product_label fs-14 ' style="color:black"> Pre-Oferta Finalizada</span>
                          <?php  } ?>
                                <div class="row">
                                <span class='fs-16'><b class='block'>OFERTA A SUPERAR:</b></span>
                                    <?php
                                    foreach ($listOffer as $listOffer_) {
                                    ?>
                                        <div class="col-md-<?= (count($listOffer) <= 2) ? '12' : '6' ?> col-xs-<?= (count($listOffer) <= 2) ? '12' : '6' ?>">
                                            <div class="<?= (count($listOffer) == 1) ? 'fs-20' : 'fs-14' ?>" style="margin-top: 1px; margin-bottom: 1px">
                                                <?= (count($listOffer) == 1) ? "$" . $listOffer_['price'] . "</span>" : "<b>" . $listOffer_['rp'] . ":</b> $" . $listOffer_['price']  ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
<?php
        }
    }
}
