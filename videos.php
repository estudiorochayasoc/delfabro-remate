<?php
require_once "Config/Autoload.php";
require_once "vendor/autoload.php";

use JasonGrimes\Paginator;

Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$videos = new Clases\Videos();
$categoria = new Clases\Categorias();

#Variables GET
$categoriaGET = isset($_GET["categoria"]) ? $f->antihack_mysqli($_GET["categoria"]) : null;
$subcategoriaGET = isset($_GET["subcategoria"]) ? $f->antihack_mysqli($_GET["subcategoria"]) : null;
$buscarGET = isset($_GET["buscar"]) ? $f->antihack_mysqli($_GET["buscar"]) : null;

#List de videos
$videosArray = $videos->list("", "", "");
#List de últimos videos
$videosArraySide = $videos->list("", "", 8);
#List de categorias
$categoriasArray = $categoria->list(["area = 'videos'"], "", "");

#Se filtra el array de videos por categoria, subcategoria y busqueda
$videosArray = $videos->filterArray($categoriaGET, $subcategoriaGET, $buscarGET, $videosArray);

#Opciones del paginador
$itemsPerPage = 12;
$totalItems = count($videosArray);
$currentPage = isset($_GET["pagina"]) ? $f->antihack_mysqli($_GET["pagina"]) : 1;
$urlPattern = preg_replace("/\/pagina\/(\d+)/i", "", CANONICAL) . '/pagina/(:num)';
$urlPattern = str_replace("videos//", "videos/", $urlPattern);

$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
$paginator->setMaxPagesToShow(10);
$paginator->setPreviousText("Anterior");
$paginator->setNextText("Siguiente");

#Con las opciones del paginador se limita la cantidad de videos a mostrar en cada página
$videosArray = array_slice($videosArray, (($currentPage - 1) * $itemsPerPage), $itemsPerPage);

#Información de cabecera
$template->set("title", "Videos | " . TITULO);
$template->set("description", "");
$template->set("keywords", "");
$template->themeInit();
?>
<!-- start page-title -->
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <h2>Videos</h2>
                <ol class="breadcrumb">
                    <li><a href="<?= URL ?>">Inicio</a></li>
                    <li>Videos</li>
                </ol>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>
<!-- end page-title -->

<section class="section-padding services-grid-section">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <div class="services-grids services-grid-view">
                    <?php foreach ($videosArray as $key => $videoItem) {
                        $link = URL . '/video/' . $f->normalizar_link($videoItem["data"]["titulo"]) . '/' . $videoItem["data"]["cod"];
                    ?>
                        <div class="grid">
                            <div class="inner mk-bg-img">
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= substr($videoItem['data']['link'], -11) ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <div class="details ">
                                    <div class="info">
                                        <div>
                                            <a href="<?= $link ?>">
                                                <h3 class="title-video-overflow"><?= $videoItem["data"]["titulo"] ?></h3>
                                            </a>
                                        </div>
                                        <p><a href="<?= $link ?>" class="more">Ver video</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="pagination-wrapper">
                    <div class="col-md-12 text-center"><?= $paginator; ?></div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php $template->themeEnd() ?>