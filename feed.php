<?php
require_once "Config/Autoload.php";
Config\Autoload::run();
$template = new Clases\TemplateSite();

#Información de cabecera
$template->set("title", "Comunidad Digital | " . TITULO);
$template->set("description", 'Sección donde vas a encontrar las últimas novedades de nuestras redes.');
// $template->set("keywords", "comunidad digital, redes sociales bremocor san francisco");
$template->themeInit();
?>

<!-- start page-title -->
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <h2>Comunidad Digital</h2>
                <ol class="breadcrumb">
                    <li><a href="<?= URL ?>">Inicio</a></li>
                    <li>Comunidad Digital</li>
                </ol>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>
<!-- end page-title -->
<!-- start blog-with-sidebar -->
<section class="blog-with-sidebar section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-md-12">
               <!-- Place <div> tag where you want the feed to appear -->
<div id="curator-feed-default-feed-layout"></div>
<!-- The Javascript can be moved to the end of the html page before the </body> tag -->
<script type="text/javascript">
/* curator-feed-default-feed-layout */
(function(){
var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
i.src = "https://cdn.curator.io/published/cb1e0516-d7c3-4b7d-abd3-8c4cf34fb110.js";
e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);
})();
</script>
            </div>
        </div>
    </div>
</section>
<?php
$template->themeEnd();
?>

<script>
    function traducir() {
        try {
            document.getElementsByClassName('crt-load-more')[0].children[0].innerText = 'Cargar más';
        } catch (err) {
            setTimeout(traducir, 1000);
        }
    }
    traducir();
</script>