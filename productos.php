<?php
require_once "Config/Autoload.php";
Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$producto = new Clases\Productos();
$categoria = new Clases\Categorias();
$subcategoria = new Clases\Subcategorias();

#Variables GET
$tituloGet = $f->antihack_mysqli(isset($_GET["titulo"]) ? str_replace("-", " ", $_GET["titulo"]) : '');
$categoriaGet = $f->antihack_mysqli(isset($_GET["categoria"]) ? str_replace("-", " ", $_GET["categoria"]) : '');
$subcategoriaGet = $f->antihack_mysqli(isset($_GET["subcategoria"]) ? str_replace("-", " ", $_GET["subcategoria"]) : '');

#List de categorías del área productos
$categoriasData = $categoria->listIfHave('productos');

#Información de cabecera
$template->set("title", "Remate Laguna Limpia | " . TITULO);
$template->set("description", "Remate Delfabro");
$template->set("keywords", "Remate Delfabro");
$template->themeInit();
?>


<div class="pa-breadcrumb container-fluid" style="background:  url(<?= URL . "/" . "assets/images/header.jpg" ?>)"></div>

<div class="container">
    <div class="shop-area mt-50 pb-90">
        <div class="row flex-row-reverse">
            <div class="col-lg-9">

                <div class="pull-right hidden-md-up" style="width: 100%">
                    <button style="margin-bottom: 50px;padding: 20px;width: 100%;!important; background-color: #F1C40F !important; border-color: white !important;" id="filter-button" class="btn btn-primary" onclick="$('#filters').show();"> <b>VER FILTROS</b></button>
                </div>

                <section class="products-section shop ">
                    <div class="row products-grids" data-url="<?= URL ?>" id="grid-products"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center" id="grid-products-loader">
                                <button id="grid-products-btn" class="btn btn-lg" onclick="loadMore()">
                                    CARGAR MÁS
                                </button>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <div class="col-lg-3 productos-categorias-order" id="filters">
                <form id="filter-form" onsubmit="event.preventDefault();getData()">
                    <div class="sidebar-wrapper">
                        <div class="sidebar-widget">
                            <h4 class="text-uppercase mb-10 mt-20 fs-20" style="color: #000;">
                                Filtros
                                <span class="btn btn-custom pull-right hidden-md-up" onclick="$('#filters').hide();" style="margin-top: -10px;padding: 0 12px"><i class="fa fa-times-circle"></i> CERRAR</span>
                                <span class="btn btn-custom pull-right hidden-md-up" onclick="$('#filters').hide();" style="margin-top: -10px;margin-right: 4px;padding: 0 12px"><i class="fa fa-check-circle"></i> APLICAR</span>
                                <hr style="border-top:2px solid #F1C40F;margin-top:5px;" />
                            </h4>
                            <div class="sidebar-search mb-20 mt-20">
                                <div class="sidebar-search-form">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" value="<?= (!empty($tituloGet)) ? $tituloGet : '' ?>" name="title" placeholder="Buscar...">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" class="btn-custom btn btn-block btn-sm fs-15">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <label style="color: #000;">ORDENAR POR:</label>
                            <select class="form-control" onchange="orderBy(this.value)">
                                <option value="1">
                                    Últimos
                                </option>
                                <option value="2">
                                    Precio menor a mayor
                                </option>
                                <option value="3">
                                    Precio mayor a menor
                                </option>
                            </select> -->
                        </div>
                        <div class="sidebar-widget shop-sidebar-border mt-20">
                            <div class="sidebar-widget-list mt-20">
                                <h4 class="text-uppercase mb-10 mt-20">
                                    Categorías
                                    <hr style="border-top:2px solid #F1C40F;margin-top:5px;" />
                                </h4>
                                <ul>
                                    <?php
                                    if (!empty($categoriasData)) {
                                        foreach ($categoriasData as $key => $cat) {
                                    ?>
                                            <li class="list-style-none mb-10 text-uppercase ">
                                                <div class="sidebar-widget-list-left container-ch" onclick="window.location.assign('<?= URL ?>/productos/c/<?= $f->normalizar_link($cat['data']['titulo']) ?>/<?= $cat['data']['cod'] ?>')">
                                                    <input id="cat-<?= $key ?>" value="<?= $cat['data']['cod'] ?>" <?= ($categoriaGet == $cat["data"]["cod"]) ? 'checked' : '' ?> name="categories[]" type="checkbox" class="check">
                                                    <label for="cat-<?= $key ?>" onclick="$('#<?= $cat['data']['cod'] ?>SubCat').toggle()" class="text-uppercase fs-18"><?= $cat['data']['titulo'] ?></label>
                                                    <span class="checkmark"></span>
                                                </div>
                                                <ul id="<?= $cat['data']['cod'] ?>SubCat" class="pl-20" style="<?= ($categoriaGet == $cat["data"]["cod"]) ? '' : 'display:none' ?>">
                                                    <?php foreach ($cat["subcategories"] as $key_ => $sub) { ?>
                                                        <li class="list-style-none fs-14">
                                                            <div class="sidebar-widget-list-left container-ch fs-14">
                                                                <input id="sub-<?= $key ?>-<?= $key_ ?>" value="<?= $sub['data']['cod'] ?>" <?= ($subcategoriaGet == $sub['data']['cod']) ? 'checked' : '' ?> class="check" name="subcategories[]" type="checkbox" onchange="getData()">
                                                                <label for="sub-<?= $key ?>-<?= $key_ ?>" class="text-uppercase fs-14"><?= $sub['data']['titulo'] ?></label>
                                                                <span class="checkmark" onclick="checkRefresh('sub-<?= $key ?>-<?= $key_ ?>')"></span>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                    <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div style="position:fixed;bottom:40px;width:90%">
                            <div class="btn btn-custom btn-block hidden-md-up mt-20 mr-10" onclick="$('#filters').hide();"><i class="fa fa-check-circle"></i> APLICAR FILTROS</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<?php
$template->themeEnd();
?>
<script src="<?= URL ?>/assets/js/services/products.js"></script>
<script>
    function checkRefresh(id) {
        if ($("#" + id).prop('checked')) {
            $("#" + id).prop("checked", false);
        } else {
            $("#" + id).prop("checked", true);
        }
        $("#" + id).trigger("change");
    }
</script>