<?php
require_once "Config/Autoload.php";
Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$portfolios = new Clases\Portfolio();
$categoria = new Clases\Categorias();

#Variables GET
$cod = $f->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');

#View del portfolio actual
$portfolios->set("cod", $cod);
$portfolioData = $portfolios->view();

#Redireccionar si no existe el portfolio actual
empty($portfolioData['data']) ? $f->headerMove(URL . '/portfolios') : null;

#List de últimos portfolios
$portfoliosArraySide = $portfolios->list("", "", 8);
#List de categorias
$categoriasArray = $categoria->list(["area = 'portfolio'"], "", "");

#Fecha normalizada del portfolio actual
$fecha = strftime("%u de %B de %Y", strtotime($portfolioData['data']['fecha']));

#Información de cabecera
$template->set("title", ucfirst(mb_strtolower($portfolioData['data']['titulo'])) . " | " . TITULO);
$template->set("description", mb_substr(strip_tags($portfolioData['data']['desarrollo'], '<p><a>'), 0, 160));
$template->set("keywords", strip_tags($portfolioData['data']['keywords']));
$template->set("imagen", isset($portfolioData['images'][0]['ruta']) ? URL . '/' . $portfolioData['images'][0]['ruta'] : LOGO);
$template->themeInit();
?>

<!-- start blog-with-sidebar -->
<section class="blog-single section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 col-md-9 blog-single-content">
                <div class="post">
                    <div class="post-title-meta pt-40">
                        <h2><?= $portfolioData['data']['titulo'] ?></h2>
                        <ul>
                            <li><a href="#"><?= $fecha ?></a></li>
                        </ul>
                    </div>
                    <div class="media">
                        <?php if (!empty($portfolioData['images'])) { ?>
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php foreach ($portfolioData['images'] as $key => $img) { ?>
                                        <div class="item <?= ($key == 0) ? "active" : null ?>">
                                            <div style="height:500px;background:url(<?= URL ?>/<?= $img['ruta'] ?>)center/cover;"></div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if (count($portfolioData['images']) > 1) { ?>
                                    <a class="left carousel-control" href="#carouselExampleControls" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Anterior</span>
                                    </a>
                                    <a class="right carousel-control" href="#carouselExampleControls" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Siguiente</span>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="post-body">
                        <?= $portfolioData['data']['desarrollo'] ?>
                    </div>
                </div> <!-- end post -->

                <div class="tag-share">
                    <div>
                        <span>Tags: </span>
                        <ul class="tag">
                            <?php foreach (explode(",", $portfolioData['data']['keywords']) as $tag) { ?>
                                <li><a href="#"><?= $tag ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div>
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_email"></a>
                            <a class="a2a_button_google_gmail"></a>
                            <a class="a2a_button_whatsapp"></a>
                        </div>
                        <script async src="https://static.addtoany.com/menu/page.js"></script>
                    </div>
                </div>

                <div>
                    <div class="fb-comments" data-href="<?= URL . '/portfolio/' . $f->normalizar_link($portfolioData['data']['titulo']) . '/' . $portfolioData['data']['cod'] ?>" data-width="100%" data-numposts="5" data-order-by="social"></div>
                </div>
            </div> <!-- end blog-content -->

            <div class="blog-sidebar col col-lg-3 col-lg-offset-1 col-md-3 col-sm-5">
                <div class="widget search-widget">
                    <form class="form" method="get" action="<?= URL ?>/portfolios">
                        <input oninvalid="this.setCustomValidity('Únicamente se permiten letras y números.')" oninput="this.setCustomValidity('')" type="text" name="buscar" pattern="[^()/><\][\\\x22#'´`¨{}^*,;.|!¡?¿$%&]+" class="form-control" placeholder="Buscar..">
                    </form>
                </div>
                <div class="widget category-widget">
                    <h3>Categorías</h3>
                    <ul>
                        <?php foreach ($categoriasArray as $categoriaItem) { ?>
                            <li>
                                <a href="<?= URL . '/portfolios/' . $f->normalizar_link($categoriaItem["data"]["titulo"]) ?>">
                                    <?= $categoriaItem["data"]["titulo"] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="widget recent-post-widget">
                    <h3>Recientes</h3>
                    <ul>
                        <?php
                        foreach ($portfoliosArraySide as $portfolioItem) {
                            $img = isset($portfolioItem['images'][0]['ruta']) ? $portfolioItem['images'][0]['ruta'] : 'assets/archivos/sin_imagen.jpg';
                            $img = URL . '/' . $img;
                            $fecha = strftime("%u de %B de %Y", strtotime($portfolioItem['data']['fecha']));
                            $link = URL . '/portfolio/' . $f->normalizar_link($portfolioItem['data']["titulo"]) . '/' . $portfolioItem['data']['cod'];
                        ?>
                            <li>
                                <div class="post-pic" style="background: url(<?= $img ?>)center/cover no-repeat; height: 67px;"></div>
                                <div class="details">
                                    <span><?= $fecha ?></span>
                                    <h4 class="title-blog-side-overflow"><a href="<?= $link ?>"><?= mb_substr($portfolioItem['data']['titulo'], 0, 60); ?></a></h4>
                                </div>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!-- end blog-with-section -->

<?php $template->themeEnd() ?>