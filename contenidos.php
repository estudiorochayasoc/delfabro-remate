<?php
require_once "Config/Autoload.php";
Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$contenidos = new Clases\Contenidos();

#Variables GET
$titulo = isset($_GET["id"]) ? $f->antihack_mysqli($_GET["id"]) : '';

#Se reemplazan los guiones con espacios para comparar en base de datos
$titulo = str_replace("-", " ", $titulo);

#List de contenidos (al ser único el título, solo trae un resultado)
$contenidoData = $contenidos->list(array("titulo = '$titulo'"), "", "");

#Si se encontro el contenido se almacena y sino se redirecciona al inicio
!empty($contenidoData) ? $contenidoData = $contenidoData[0] : $f->headerMove(URL);

#Información de cabecera
$template->set("title", $contenidoData['data']['titulo'] . " | " . TITULO);
$template->set("description", substr(strip_tags($contenidoData['data']['contenido'], '<p><a>'), 0, 120));
$template->set("keywords", " verduleria san francisco, san francisco córdoba  verduleria, frutas y verduras san francisco, verduleria online");
$template->set("imagen", isset($contenidoData['images'][0]['ruta']) ? URL . '/' . $contenidoData['images'][0]['ruta'] : LOGO);
$template->themeInit();
?>
<!-- start page-title -->
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <h2><?= $contenidoData['data']['titulo'] ?></h2>
                <ol class="breadcrumb">
                    <li><a href="<?= URL ?>">Inicio</a></li>
                    <li><?= $contenidoData['data']['titulo'] ?></li>
                </ol>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>
<!-- end page-title -->

<!-- start offer -->
<section class="section-padding offer-section">
    <div class="container">
        <div class="row">
            <div class="col col-md-12">
                <div class="section-title-s3">
                    <h2><?= $contenidoData['data']['subtitulo'] ?></h2>
                </div>
                <div class="offer-text">
                    <?= $contenidoData['data']['contenido'] ?>
                </div>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>
<!-- end offer -->

<?php
$template->themeEnd();
?>