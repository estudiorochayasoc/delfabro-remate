<?php
$usuario = new Clases\Usuarios();
$ofertas = new Clases\Ofertas();
$pedido = isset($_GET["pedido"]) ? $funciones->antihack_mysqli($_GET["pedido"]) : '0';
$f = new Clases\PublicFunction();
$enviar = new Clases\Email();
$config = new Clases\Config();

$emailData = $config->viewEmail();

?>
    <div class="mt-20">
        <div class="col-lg-12 col-md-12">
            <h4>Usuarios
                <a class="btn btn-success pull-right" href="<?= URL_ADMIN ?>/index.php?op=usuarios&accion=agregar<?php if ($pedido == 1) {
                    echo '&pedido=1';
                } ?>">
                    AGREGAR
                    USUARIOS
                </a>
            </h4>
            <hr/>
            <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
            <hr/>
            <?php
            if ($pedido == 1) {
                ?>
                <div class="alert alert-success" role="alert">
                    Seleccion un usuario para comenzar a armar el pedido o agrega un usuario nuevo.
                </div>
                <?php
            }
            ?>
            <table class="table  table-bordered  ">
                <thead>
                <th>Nombre</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Ajustes</th>
                </thead>
                <tbody>
                <?php
                $filter = array();
                $usuariosData = $usuario->list('', '', '');
                if (is_array($usuariosData)) {
                    foreach ($usuariosData as $data) {
                        ?>
                        <tr>
                            <td><?= mb_strtoupper($data['data']["nombre"]) . " " . mb_strtoupper($data['data']["apellido"]) ?></td>

                            <td><?= mb_strtolower($data['data']["email"]) ?></td>
                            <td>
                            <?= mb_strtolower($data['data']["telefono"]) ?>
                            </td>
                            <td>
                                <?php
                                if ($data['data']["estado"] == 1) {
                                    ?>
                                    <a class="btn btn-success"
                                       data-toggle="tooltip"
                                       data-placement="top"
                                       title="Verificado"
                                       href="<?= URL_ADMIN . '/index.php?op=usuarios&cod=' . $data['data']['cod'] . '&active=0' ?>">
                                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    <a class="btn btn-primary"
                                       data-toggle="tooltip"
                                       data-placement="top"
                                       title="No verificado"
                                       href="<?= URL_ADMIN . '/index.php?op=usuarios&cod=' . $data['data']['cod'] . '&active=1' ?>">
                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                    </a>
                                    <?php
                                }
                                ?>
                                <span>
                            </span>
                                <a class="btn btn-warning"
                                   data-toggle="tooltip"
                                   data-placement="top"
                                   title="Ver ofertas"
                                   href="<?= URL_ADMIN ?>/index.php?op=ofertas&accion=ver&usuario=<?= $data['data']["cod"] ?>">
                                    <i class="fa fa-list"></i></a>

                                <a class="btn btn-info"
                                   data-toggle="tooltip"
                                   data-placement="top"
                                   title="Modificar"
                                   href="<?= URL_ADMIN ?>/index.php?op=usuarios&accion=modificar&cod=<?= $data['data']["cod"] ?>">
                                    <i class="fa fa-cog"></i></a>
                                
                                <a class="btn btn-danger"
                                   data-toggle="tooltip"
                                   data-placement="top"
                                   title="Eliminar"
                                   href="<?= URL_ADMIN ?>/index.php?op=usuarios&borrar=<?= $data['data']["cod"] ?>">
                                    <i class="fa fa-trash"></i></a>
                                
                            </td>
                        </tr>
                        <?php

                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
<?php
if (isset($_GET["borrar"])) {
    $cod = isset($_GET["borrar"]) ? $funciones->antihack_mysqli($_GET["borrar"]) : '';
    $usuario->set("cod", $cod);
    $usuario->delete();
    $funciones->headerMove(URL_ADMIN . "/index.php?op=usuarios");
}
if (isset($_GET["active"])) {
    $estado = isset($_GET["active"]) ? $funciones->antihack_mysqli($_GET["active"]) : '';
    $usuario->set("cod", isset($_GET["cod"]) ? $funciones->antihack_mysqli($_GET["cod"]) : '');   
    $usuario->editEstado("estado", $_GET["active"]);
    $ofertas->editEstado($_GET["active"], $_GET["cod"]);
        if ($_GET["active"] == 1) {
            $usuarioData = $usuario->view();
        //Envio de mail al usuario
        $mensaje = "            
        <p>Estimado " . $usuarioData['data']['nombre'] . " " . $usuarioData['data']['apellido'] . ",</p>
        <p>Se a validado toda la informacion correctamente.</p>
        <p>Ya puede entrar <a href='https://delfabro.com.ar/remate/productos/'><u>AQUI</u></a> y comenzar a realizar pre-ofertas</p>
        <p>Atte. Delfabro Agropecuaria</p>
        ";
        $asunto = "Cuenta Validada - Delfabro Agropecuaria";
        $enviar->set("asunto", $asunto);
        $enviar->set("receptor", $usuarioData['data']['email']);
        $enviar->set("emisor", $emailData['data']['remitente']);
        $enviar->set("mensaje", $mensaje);
        $enviar->emailEnviar();
        }
    $funciones->headerMove(URL_ADMIN . "/index.php?op=usuarios");
}
?>
