<?php
$productos = new Clases\Productos();
$categoria = new Clases\Categorias();
$subcategoria = new Clases\Subcategorias();
$imagenes = new Clases\Imagenes();
$conexion = new Clases\Conexion();
$con = $conexion->con();
include dirname(__DIR__, 3) . "/vendor/phpoffice/phpexcel/Classes/PHPExcel.php";
require dirname(__DIR__, 3) . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php";
?>
<div class="col-md-12">
    <h3>Importar productos de Excel a la Web <button class="pull-right fs-15 btn btn-info btn-sm text-uppercase" onclick="exportTableToExcel('excel','MODELO EXCEL IMPORTAR PRODUCTOS')">descargar modelo</button></h3>
    <hr />
    <div>Las columnas del excel deben ser igual a estas en (.xls o xlsx) - <a href="#" class="bold fs-12 text-uppercase" onclick="exportTableToExcel('excel','MODELO EXCEL IMPORTAR PRODUCTOS')">descargar modelo</a></div>
    <table border="1" class="fs-12 table table-sm" id="excel">
        <thead>
            <th>TÍTULO</th>
            <th>PRECIO</th>
            <th>PRECIO MAYORISTA</th>
            <th>PRECIO DESCUENTO</th>
            <th>STOCK</th>
            <th>PESO (kg)</th>
            <th>DESCRIPCIÓN DEL PRODUCTO</th>
            <th>CATEGORIA</th>
            <th>SUBCATEGORIA</th>
            <th>CÓDIGO DEL PRODUCTO</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
            <th>DATO VARIABLE</th>
        </thead>
        <tbody class="hidden">
            <?php foreach ($productos->list("", "", "") as $product) { ?>
                <tr>
                    <td><?= $product["data"]["titulo"] ?></td>
                    <td><?= $product["data"]["precio"] ?></td>
                    <td><?= $product["data"]["precio_mayorista"] ?> </td>
                    <td><?= $product["data"]["precio_descuento"] ?> </td>
                    <td><?= $product["data"]["stock"] ?></td>
                    <td><?= $product["data"]["peso"] ?></td>
                    <td><?= $product["data"]["desarrollo"] ?></td>
                    <td><?= $product["category"]["data"]["titulo"] ?></td>
                    <td><?= $product["subcategory"]["data"]["titulo"] ?></td>
                    <td><?= $product["data"]["cod_producto"] ?></td>
                    <td><?= $product["data"]["variable1"] ?></td>
                    <td><?= $product["data"]["variable2"] ?></td>
                    <td><?= $product["data"]["variable3"] ?></td>
                    <td><?= $product["data"]["variable4"] ?></td>
                    <td><?= $product["data"]["variable5"] ?></td>
                    <td><?= $product["data"]["variable6"] ?></td>
                    <td><?= $product["data"]["variable7"] ?></td>
                    <td><?= $product["data"]["variable8"] ?></td>
                    <td><?= $product["data"]["variable9"] ?></td>
                    <td><?= $product["data"]["variable10"] ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <hr />
    <form action="index.php?op=productos&accion=importar" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <input type="file" name="excel" class="form-control" required />
            </div>
            <div class="col-md-6 mt-10">
                <input type="submit" name="submit" value="Verifica e importar archivo" class='btn  btn-info' />
            </div>
        </div>
    </form>

    <hr />

    <?php
    if (isset($_POST['submit'])) {
        if (isset($_FILES['excel']['name']) && $_FILES['excel']['name'] != "") {
            $objPHPExcel = PHPEXCEL_IOFactory::load($_FILES['excel']['tmp_name']);
            $sheet = $objPHPExcel->getActiveSheet()->toArray();
            foreach ($sheet as $cellVal) {
                if (!empty($cellVal[7])) {
                    //CATEGORIA
                    $cellVal[7] = mb_strtoupper($cellVal[7]);
                    $categoriaSearch = $categoria->list(["titulo='" . $cellVal[7] . "'", "area='productos'"], '', 1);
                    if (!empty($categoriaSearch)) {
                        $categoria_cod = $categoriaSearch[0]['data']['cod'];
                    } else {
                        $categoria_cod = substr(md5(uniqid(rand())), 0, 11);
                        $categoria->set("cod", $categoria_cod);
                        $categoria->set("titulo", $cellVal[7]);
                        $categoria->set("descripcion", $cellVal[7]);
                        $categoria->set("area", "productos");
                        $categoria->add();
                    }
                    //SUBCATEGORIA
                    if (!empty($cellVal[8])) {
                        $cellVal[8] = mb_strtoupper($cellVal[8]);
                        $subcategoriaSearch = $subcategoria->list(["titulo='" . $cellVal[8] . "'", "categoria='" . $categoria_cod . "'"], '', 1);
                        if (!empty($subcategoriaSearch)) {
                            $subcategoria_cod = $subcategoriaSearch[0]['data']['cod'];
                        } else {
                            $subcategoria_cod = substr(md5(uniqid(rand())), 0, 11);
                            $subcategoria->set("cod", $subcategoria_cod);
                            $subcategoria->set("categoria", $categoria_cod);
                            $subcategoria->set("titulo", $cellVal[8]);
                            $subcategoria->add();
                        }
                    }
                }

                $productos->set("meli", '');
                $productos->set("url", '');
                $productos->set("keywords", $cellVal[0]);
                $productos->set("description", $cellVal[0]);

                $productos->set("titulo", $cellVal[0]); //TÍTULO
                $productos->set("precio", $cellVal[1]); //PRECIO
                $productos->set("precio_mayorista", $cellVal[2]); //PRECIO MAYORISTA
                $productos->set("precio_descuento", $cellVal[3]); //PRECIO DESCUENTO
                $productos->set("stock", $cellVal[4]); //STOCK
                $productos->set("peso", $cellVal[5]); //PESO (kg)
                $productos->set("categoria", $categoria_cod); //CATEGORIA
                $productos->set("subcategoria", $subcategoria_cod); //SUBCATEGORIA
                $productos->set("desarrollo", $cellVal[6]); //DESCRIPCIÓN DEL PRODUCTO
                $productos->set("cod_producto", $cellVal[9]); //CÓDIGO DEL PRODUCTO
                $productos->set("variable1", $cellVal[10]);
                $productos->set("variable2", $cellVal[11]);
                $productos->set("variable3", $cellVal[12]);
                $productos->set("variable4", $cellVal[13]);
                $productos->set("variable5", $cellVal[14]);
                $productos->set("variable6", $cellVal[15]);
                $productos->set("variable7", $cellVal[16]);
                $productos->set("variable8", $cellVal[17]);
                $productos->set("variable9", $cellVal[18]);
                $productos->set("variable10", $cellVal[19]);
                $productos->set("fecha", 'NOW()'); //CÓDIGO DEL PRODUCTO

                $checkProduct = $productos->viewByCod($cellVal[9]);

                if (isset($checkProduct["data"]["cod"])) {
                    $productos->set("cod", $checkProduct["data"]["cod"]); //COD
                    $productos->editSingle("precio", $cellVal[1]);
                    $productos->editSingle("precio_mayorista", $cellVal[2]);
                    $productos->editSingle("precio_descuento", $cellVal[3]);
                    $productos->editSingle("stock", $cellVal[4]);
                    $productos->editSingle("peso", $cellVal[5]);
                    $productos->editSingle("descripcion", $cellVal[6]);
                    $productos->editSingle("variable1", $cellVal[10]);
                    $productos->editSingle("variable2", $cellVal[11]);
                    $productos->editSingle("variable3", $cellVal[12]);
                    $productos->editSingle("variable4", $cellVal[13]);
                    $productos->editSingle("variable5", $cellVal[14]);
                    $productos->editSingle("variable6", $cellVal[15]);
                    $productos->editSingle("variable7", $cellVal[16]);
                    $productos->editSingle("variable8", $cellVal[17]);
                    $productos->editSingle("variable9", $cellVal[18]);
                    $productos->editSingle("variable10", $cellVal[19]);
                    $txt = "ACTUALIZADO";
                } else {
                    $productos->set("cod", substr(md5(uniqid(rand())), 0, 10)); //COD
                    $productos->add();
                    $txt = "AGREGADO";
                }
                echo "<div class='alert alert-success'>$cellVal[0] - $txt </div>";
            }
        }
    }
    ?>
</div>