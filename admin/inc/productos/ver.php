<?php
$producto = new Clases\Productos();
$funciones = new Clases\PublicFunction();
$categorias = new Clases\Categorias();
$subcategorias = new Clases\Subcategorias();

$pagina = $funciones->antihack_mysqli(isset($_GET["pagina"]) ? $_GET["pagina"] : '0');
$filter = array();

if ($pagina > 0) {
    $pagina = $pagina - 1;
}

if (@count($filter) == 0) {
    $filter = '';
}

if (@count($_GET) == 0) {
    $anidador = "?";
} else {
    if ($pagina >= 0) {
        $anidador = "&";
    } else {
        $anidador = "?";
    }
}

$productos = $producto->list(isset($_GET['cod']) ? ["cod = '" . $_GET['cod'] . "'"] : "", "", (100 * $pagina) . ',' . 100, true);
$productoPaginador = $producto->paginador("", 100);
?>
<div class="mt-20">
    <div class="col-lg-12 col-md-12">
        <h4>Productos <a class="btn btn-success pull-right" href="<?= URL_ADMIN ?>/index.php?op=productos&accion=agregar">AGREGAR PRODUCTOS</a></h4>
        <hr />
        <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
        <hr />
        <table class="table  table-bordered  ">
            <thead>
                <th>Título</th>
                <th>Precio</th>
                <th>Categoria</th>
                <th>Subcategoria</th>
                <th>Finalizar Remate</th>
                <th>Mostrar en Web</th>
                <th>Ajustes</th>
            </thead>
            <tbody>
                <?php
                if (is_array($productos)) {
                    foreach ($productos as $producto_) {
                        $cod = $producto_["data"]["cod"];
                        $categorias->set("cod",  $producto_["data"]["categoria"]);
                        $categoriaData = $categorias->view();
                        $subcategorias->set("cod",  $producto_["data"]["subcategoria"]);
                        $subcategoriaData = $subcategorias->view();
                        

                        $txtCod = '';
                        $txtCod .= !empty($producto_["data"]["cod_producto"]) ? "<br/><b>COD:</b> " . $producto_["data"]["cod_producto"] : "";
                ?>
                        <tr>
                            <td width="200">
                                <input class="borderInputBottom" style='width:auto' onchange='editProduct("titulo-<?= $cod ?>","<?= URL_ADMIN ?>")' id='titulo-<?= $cod ?>' name='titulo' value='<?= $producto_["data"]["titulo"] ?>' />
                                <?= "<span style='font-size:11px'>" . $txtCod . "</span>" ?>
                            </td>
                            <td width="200">$ <input class="borderInputBottom" style='width:auto' onchange='editProduct("precio-<?= $cod ?>","<?= URL_ADMIN ?>")' id='precio-<?= $cod ?>' name='precio' value='<?= $producto_["data"]["precio"] ?>' /></td>
                            <td width="200"><input class="borderInputBottom" style='width:auto' name='categoria' value='<?= $categoriaData['data']['titulo'] ?>' /></td>
                            <td width="200"><input class="borderInputBottom" style='width:auto' name='subcategoria' value='<?= $subcategoriaData['data']['titulo'] ?>' /></td>
                            
                            <td width="80" class="text-left">
                                <input type="checkbox" class="borderInputBottom" style='width:auto' onchange='editProduct("variable8-<?= $cod ?>","<?= URL_ADMIN ?>")' id='variable8-<?= $cod ?>' name='variable8' value='<?= ($producto_["data"]["variable8"] == 1) ? 0 : 1 ?>' <?= ($producto_["data"]["variable8"] == 1) ? "checked" : ''; ?> />
                            </td>
                            <td width="80" class="text-left">
                                <input type="checkbox" class="borderInputBottom" style='width:auto' onchange='editProduct("variable9-<?= $cod ?>","<?= URL_ADMIN ?>")' id='variable9-<?= $cod ?>' name='variable9' value='<?= ($producto_["data"]["variable9"] == 1) ? 0 : 1 ?>' <?= ($producto_["data"]["variable9"] == 1) ? "checked" : ''; ?> />
                            </td>
                            <td width="120" class="text-left">
                                <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Modificar" href="<?= URL_ADMIN ?>/index.php?op=productos&accion=modificar&cod=<?= $producto_["data"]["cod"] ?>">
                                    <i class="fa fa-cog"></i>
                                </a>
                                <a class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" href="<?= URL_ADMIN ?>/index.php?op=productos&accion=ver&borrar=<?= $producto_["data"]["cod"] ?>">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <nav aria-label="Page navigation">
            <ul class="pagination ">
                <?php
                if ($productoPaginador != 1 && $productoPaginador != 0) {
                    $url_final = $funciones->eliminar_get(CANONICAL, "pagina");
                    $links = '';
                    $links .= "<li class='page-item' ><a class='page-link' href='" . $url_final . $anidador . "pagina=1'>1</a></li>";
                    $i = max(2, $pagina - 5);

                    if ($i > 2) {
                        $links .= "<li class='page-item' ><a class='page-link' href='#'>...</a></li>";
                    }
                    for (; $i <= min($pagina + 35, $productoPaginador); $i++) {
                        $links .= "<li class='page-item' ><a class='page-link' href='" . $url_final . $anidador . "pagina=" . $i . "'>" . $i . "</a></li>";
                    }
                    if ($i - 1 != $productoPaginador) {
                        $links .= "<li class='page-item' ><a class='page-link' href='#'>...</a></li>";
                        $links .= "<li class='page-item' ><a class='page-link' href='" . $url_final . $anidador . "pagina=" . $productoPaginador . "'>" . $productoPaginador . "</a></li>";
                    }
                    echo $links;
                    echo "";
                }
                ?>
            </ul>
        </nav>
    </div>
</div>
<?php
if (isset($_GET["borrar"])) {
    $cod = $funciones->antihack_mysqli(isset($_GET["borrar"]) ? $_GET["borrar"] : '');
    $producto->set("cod", $cod);
    $productos = $producto->view();
    if (!empty($productos['data']['meli'])) {
        $producto->meli = $productos['data']['meli'];
        $producto->deleteMeli();
    }
    $producto->delete();
    $funciones->headerMove(URL_ADMIN . "/index.php?op=productos");
}
?>