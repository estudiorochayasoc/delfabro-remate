<?php
require '../vendor/autoload.php';
$productos = new Clases\Productos();
$imagenes = new Clases\Imagenes();
$zebra = new Clases\Zebra_Image();
$categoria = new Clases\Categorias();
$atributo = new Clases\Atributos();
$subatributo = new Clases\Subatributos();
$combinacion = new Clases\Combinaciones();
$detalleCombinacion = new Clases\DetalleCombinaciones();


$cod = isset($_GET["cod"]) ? $funciones->antihack_mysqli($_GET["cod"]) : '';
$borrarImg = isset($_GET["borrarImg"]) ? $funciones->antihack_mysqli($_GET["borrarImg"]) : '';
$categoriasData = $categoria->list(array("area = 'productos'"), "titulo ASC", "");

$productos->set("cod", $cod);
$producto = $productos->view(null, true);
$atributo->set("productoCod", $producto['data']['cod']);
$atributosArray = $atributo->list();

//CAMBIAR ORDEN DE LAS IMAGENES
if (isset($_GET["ordenImg"]) && isset($_GET["idImg"])) {
    $imagenes->set("id", $_GET["idImg"]);
    $imagenes->set("orden", $_GET["ordenImg"]);
    $imagenes->setOrder();
    $funciones->headerMove(URL_ADMIN . "/index.php?op=productos&accion=modificar&cod=$cod");
}

//BORRAR IMAGEN
if ($borrarImg != '') {
    $imagenes->set("id", $borrarImg);
    $imagenes->delete();
    $funciones->headerMove(URL_ADMIN . "/index.php?op=productos&accion=modificar&cod=$cod");
}

if (isset($_POST["agregar"])) {
    $count = 0;
    $cod = $producto["data"]["cod"];
    $productos->set("id", $producto["data"]["id"]);
    $productos->set("cod", $cod);
    $productos->set("titulo", isset($_POST["titulo"]) ? $funciones->antihack_mysqli($_POST["titulo"]) : '');
    $productos->set("cod_producto", isset($_POST["cod_producto"]) ? $funciones->antihack_mysqli($_POST["cod_producto"]) : '');
    $productos->set("precio", isset($_POST["precio"]) ? $funciones->antihack_mysqli($_POST["precio"]) : '');
    $productos->set("precio_descuento", isset($_POST["precio_descuento"]) ? $funciones->antihack_mysqli($_POST["precio_descuento"]) : '');
    $productos->set("precio_mayorista", isset($_POST["precio_mayorista"]) ? $funciones->antihack_mysqli($_POST["precio_mayorista"]) : '');
    $productos->set("peso", isset($_POST["peso"]) ? $funciones->antihack_mysqli($_POST["peso"]) : 0);
    $stock = isset($_POST["stock"]) ? $funciones->antihack_mysqli($_POST["stock"]) : 1;
    $productos->set("stock", $stock);
    $productos->set("desarrollo", isset($_POST["desarrollo"]) ? $funciones->antihack_mysqli($_POST["desarrollo"]) : '');
    $productos->set("categoria", isset($_POST["categoria"]) ? $funciones->antihack_mysqli($_POST["categoria"]) : '');
    $productos->set("subcategoria", isset($_POST["subcategoria"]) ? $funciones->antihack_mysqli($_POST["subcategoria"]) : '');
    $productos->set("keywords", isset($_POST["keywords"]) ? $funciones->antihack_mysqli($_POST["keywords"]) : '');
    $productos->set("description", isset($_POST["description"]) ? $funciones->antihack_mysqli($_POST["description"]) : '');
    $productos->set("fecha", isset($_POST["fecha"]) ? $funciones->antihack_mysqli($_POST["fecha"]) : date("Y-m-d"));
    $productos->set("meli", isset($_POST["meli"]) ? $funciones->antihack_mysqli($_POST["meli"]) : '');
    $productos->set("url", isset($_POST["url"]) ? $funciones->antihack_mysqli($_POST["url"]) : '');

    // Codigo Youtube
    $productos->set("variable1", $funciones->antihack_mysqli($_POST["variable1"]));
  

    // RP INDIVIDUAL 
    if (empty($_POST["variable10"])) {
        $productos->set("variable2", $funciones->antihack_mysqli($_POST["cod_producto"]));
    } else {
    // RP POR LOTE   
        $productos->set("variable2", $funciones->antihack_mysqli($_POST["variable2"]));
    }
    $productos->set("variable3", $funciones->antihack_mysqli($_POST["variable3"]));
    $productos->set("variable4", $funciones->antihack_mysqli($_POST["variable4"]));
    $productos->set("variable5", $funciones->antihack_mysqli($_POST["variable5"]));
    $productos->set("variable6", $funciones->antihack_mysqli($_POST["variable6"]));
// Lote COmpleto
    $productos->set("variable7",$funciones->antihack_mysqli($_POST["variable7"]));

    // Finalizar Remate  
    $productos->set("variable8", isset($_POST["finRemate"]) ? $funciones->antihack_mysqli($_POST["finRemate"]) : '');

    //MOSTRAR EN WEB
    $estado = isset($_POST["estado"]) ? $_POST["estado"] : '';
    $productos->set("variable9", $estado);
    

    // BANDERA REMATE INDIVIDUAL
    $individual = isset($_POST["variable10"]) ? $_POST["variable10"] : '';
    $productos->set("variable10", $individual);


    $atributos = isset($_POST["atributos"]) ? $_POST["atributos"] : '';

    // IMAGENES
    if (isset($_FILES['files'])) {
        $imagenes->resizeImages($cod, $_FILES['files'], "../assets/archivos", "../assets/archivos/recortadas");
    }

    $error = '';

    // ATRIBUTOS PRODUCTO
    if (empty($error)) {
        $productos->edit();
        $funciones->headerMove(URL_ADMIN . '/index.php?op=productos');
    }
}
?>
<div class="col-md-12 ">
    <h4>Productos</h4>
    <hr />
    <?php
    if (!empty($error)) {
    ?>
        <div class="alert alert-danger" role="alert"><?= $error; ?></div>
    <?php
    }
    ?>
    <form method="post" class="row" enctype="multipart/form-data">
        <label class="col-md-4">Título:<br />
            <input type="text" name="titulo" value="<?= $producto["data"]["titulo"] ?>">
        </label>
        <label class="col-md-3">
            Categoría:<br />
            <select name="categoria">
                <option value="">-- categorías --</option>
                <?php
                foreach ($categoriasData as $categoria) {
                    if ($producto["data"]["categoria"] == $categoria["data"]["cod"]) {
                        echo "<option value='" . $categoria["data"]["cod"] . "' selected>" . mb_strtoupper($categoria["data"]["titulo"]) . "</option>";
                    } else {
                        echo "<option value='" . $categoria["data"]["cod"] . "'>" . mb_strtoupper($categoria["data"]["titulo"]) . "</option>";
                    }
                }
                ?>
            </select>
        </label>
        <label class="col-md-3">
            Subcategoría:<br />
            <select name="subcategoria">
                <option value="">-- Sin subcategoría --</option>
                <?php
                foreach ($categoriasData as $categoria) {
                ?>
                    <optgroup label="<?= mb_strtoupper($categoria["data"]['titulo']) ?>">
                        <?php
                        foreach ($categoria["subcategories"] as $subcategorias) {
                            if ($producto["data"]["subcategoria"] == $subcategorias["data"]["cod"]) {
                                echo "<option value='" . $subcategorias["data"]["cod"] . "' selected>" . mb_strtoupper($subcategorias["data"]["titulo"]) . "</option>";
                            } else {
                                echo "<option value='" . $subcategorias["data"]["cod"] . "'>" . mb_strtoupper($subcategorias["data"]["titulo"]) . "</option>";
                            }
                        }
                        ?>
                    </optgroup>
                <?php
                }
                ?>
            </select>
        </label>
        <label class="col-md-2">Código:<br />
            <input type="text" name="cod_producto" value="<?= $producto["data"]["cod_producto"] ?>">
        </label>
        <label class="col-md-3">Precio Inicial:<br />
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                </div>
                <input type="number" step="any" class="form-control" min="0" value="<?= $producto["data"]["precio"] ? $producto["data"]["precio"] : '' ?>" name="precio" >
            </div>
        </label>
        <label class="col-md-3">Codigo Youtube:<br />
            <input type="text" name="variable1" value="<?= isset($producto["data"]["variable1"]) ? $producto["data"]["variable1"] : ''; ?>" >
        </label>
        <div class="clearfix"></div>

        <label class="col-md-3 mt-25">¿Remate Individual?                                   
            <input type="checkbox" name="variable10" class="ml-10" id="checkRemate" value="1" <?=($producto["data"]["variable10"] == 1) ? "checked" : '' ?>>
        </label>
        <div class="clearfix"></div>
        <label class="col-md-12" id="checkSi" <?=($producto['data']['variable10'] == 1) ? "style='display: block;'" :  "style='display:none;'"?>>
        <div>
                <hr />
                <p class="bold">Ingresar R.P. Individualmente en cada casillero</p>
                <div class="row">
                    <?php for ($i = 2; $i <= 7; $i++) { ?>
                        <label class="col-md-2">R.P. <?= ($i - 1) ?>:<br />
                            <input type="text" name="variable<?= $i ?>" value="<?= isset($producto['data']["variable$i"]) ? $producto['data']["variable$i"] : ''; ?>">
                        </label>
                    <?php } ?>
                </div>
                <hr />
            </div>
        </label>
        <div class="clearfix"></div>
        <label class="col-md-12">
            <hr />
            Desarrollo:<br />
            <textarea name="desarrollo" class="ckeditorTextarea"><?= $producto["data"]["desarrollo"] ?></textarea>
        </label>
        <div class="clearfix"></div>
        <label class="col-md-12">Palabras claves dividas por ,<br />
            <input type="text" name="keywords" value="<?= $producto["data"]["keywords"] ?>">
        </label>
        <label class="col-md-12">Descripción breve<br />
            <textarea name="description"><?= $producto["data"]["description"] ?></textarea>
        </label>
        <br />
        <div class="col-md-12">
            <div class="row">
                <?php
                if (!empty($producto['images'])) {
                    foreach ($producto['images'] as $img) {
                ?>
                        <div class='col-md-2 mb-20 mt-20'>
                            <div style="height:200px;background:url(<?= '../' . $img['ruta']; ?>) no-repeat center center/contain;">
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <a href="<?= URL_ADMIN . '/index.php?op=productos&accion=modificar&cod=' . $img['cod'] . '&borrarImg=' . $img['id'] ?>" class="btn btn-sm btn-block btn-danger">
                                        BORRAR IMAGEN
                                    </a>
                                </div>
                                <div class="col-md-5 text-right">
                                    <select onchange='$(location).attr("href", "<?= CANONICAL ?>&idImg=<?= $img["id"] ?>&ordenImg="+$(this).val())'>
                                        <?php
                                        for ($i = 0; $i <= count($producto['images']); $i++) {
                                            if ($img["orden"] == $i) {
                                                echo "<option value='$i' selected>$i</option>";
                                            } else {
                                                echo "<option value='$i'>$i</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                    <i>orden</i>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>
        <label class="col-md-7">Imágenes:<br />
            <input type="file" id="file" name="files[]" multiple="multiple" accept="image/*" />
        </label>
        <br />
        <div class="clearfix"><br /></div>
        <div class="col-md-12">
            <label><input type="checkbox" name="estado" value="1" <?= ($producto["data"]["variable9"] == 1) ? "checked" : ""; ?>> Mostrar en la Web</label>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
            <label><input type="checkbox" name="finRemate" value="1" <?= ($producto["data"]["variable8"] == 1) ? "checked" : ""; ?>> Finalizar Remate</label>
        </div>
        <div class="col-md-12">
            <input type="submit" class="btn btn-primary" name="agregar" value="Modificar Producto" />
        </div>
</div>
<script>
    $(function () {
        $("#checkRemate").click(function () {
            if ($(this).is(":checked")) {
                $("#checkSi").show();
            } else {
                $("#checkSi").hide();
            }
        });
    });

    function checkAttrProducts() {
        $.ajax({
            url: "<?= URL_ADMIN ?>/inc/productos/api/atributos/atributosVer.php",
            type: "GET",
            data: {
                cod: "<?= $cod ?>"
            },
            success: function(data) {
                data = JSON.parse(data);
                $('#listAttr').html('');
                if (data.length != 0) {
                    for (i = 0; i < data.length; i++) {
                        var texto = "<strong>" + data[i]["atribute"]["value"] + ": </strong>";
                        for (o = 0; o < data[i]["atribute"]["subatributes"].length; o++) {
                            texto += data[i]["atribute"]["subatributes"][o]["value"] + " | ";
                        }
                        $('#listAttr').append("<span class='text-uppercase mt-10'>" + texto + "</span>");
                        $('#listAttr').append(
                            "<span class='ml-10  mt-10 mb-5 btn btn-warning btn-sm text-uppercase' onclick='openModal(\"<?= URL_ADMIN ?>/inc/productos/api/atributos/atributosModificar.php?cod=" + data[i]['atribute']['cod'] + "\",\"Modificar " + data[i]['atribute']['value'] + "\")'><i class='fa fa-edit'></i></span><br/>");
                    }
                }
            },
            error: function() {
                alert('Error occured');
            }
        });
    }

    function checkCombProducts() {
        $.ajax({
            url: "<?= URL_ADMIN ?>/inc/productos/api/variaciones/variacionesVer.php",
            type: "GET",
            data: {
                cod: "<?= $cod ?>"
            },
            success: function(data) {
                data = JSON.parse(data);
                $('#listComb').html('');
                if (data.length != 0) {
                    for (i = 0; i < data.length; i++) {
                        var texto = "";
                        for (o = 0; o < data[i]["combination"].length; o++) {
                            texto += data[i]["combination"][o]["value"] + " | ";
                        }
                        texto += " <strong>Precio:</strong> $" + data[i]['detail']['precio'] + " <strong>Stock:</strong> " + data[i]['detail']['stock'];
                        if (data[i]['detail']['mayorista'] > 0) {
                            texto += " <strong>Precio Mayorista:</strong> $" + data[i]['detail']['mayorista'];
                        } else {
                            texto += " <strong>Precio Mayorista:</strong> No posee";
                        }
                        $('#listComb').append("<span class='text-uppercase mt-10'>" + texto + "</span>");
                        $('#listComb').append(
                            "<span class='ml-10 mt-10 mb-5 btn btn-warning btn-sm text-uppercase' onclick='openModal(\"<?= URL_ADMIN ?>/inc/productos/api/variaciones/variacionesModificar.php?cod=" + data[i]['detail']['cod_combinacion'] + "&product=" + data[i]['product'] + "\",\"Modificar " + "\")'><i class='fa fa-edit'></i></span><br/>");
                    }
                }
            },
            error: function() {
                alert('Error occured');
            }
        });
    }

    checkAttrProducts();
    checkCombProducts();
</script>