<?php
$ofertas   = new Clases\Ofertas();
$usuarios   = new Clases\Usuarios();
$productos = new Clases\Productos();

$filter = [];
isset($_GET['usuario']) ? $filter[] = "usuario = '" . $_GET['usuario'] . "'" : '';
isset($_GET['rp']) ? $filter[] = "rp = '" . $_GET['rp'] . "'" : '';
$filter = count($filter) ? $filter : '';
//isset($_POST['buscar']) ? $filter[] = "buscar = '" . $_POST['buscar'] . "'" : '';
$data = $ofertas->list($filter, '', '');
?>
<div class="mt-20">
    <div class="col-lg-12 col-md-12">
        <h4>
            OFERTAS
        </h4>
        <hr />
        <input class="form-control" type="text" placeholder="Buscar..">
        <hr />
        <table class="table  table-bordered  ">
            <thead>
                <th>
                    Fecha
                </th>
                <th>
                    R.P.
                </th>
                <th>
                    Oferta
                </th>
                <th>
                    Producto
                </th>
                <th>
                    Usuario
                </th>
                <th>
                </th>
            </thead>
            <tbody>
                <?php
                if (is_array($data)) {
                    foreach ($data as $data_) {
                        $productos->set("cod", $data_["producto"]);
                        $productoData = $productos->view();
                        $usuarios->set("cod", $data_["usuario"]);
                        $usuariosData = $usuarios->view();

                        echo "<tr>";
                        echo "<td>" . strtoupper($data_["fecha"]) . "</td>";
                        echo "<td><a href='" . URL_ADMIN . "/index.php?op=ofertas&accion=ver&rp=" . ($data_["rp"]) . "'>" . strtoupper($data_["rp"]) . "</a></td>";
                        echo "<td>$ " . strtoupper($data_["oferta"]) . "</td>";
                        echo "<td><a href='" . URL_ADMIN . "/index.php?op=productos&accion=ver&cod=" . $data_["producto"] . "'>" . strtoupper($productoData['data']['titulo']) . "</a></td>";
                        echo "<td><a href='" . URL_ADMIN . "/index.php?op=ofertas&accion=ver&usuario=" . $data_["usuario"] . "'>" . strtoupper($usuariosData["data"]["nombre"] . " " . $usuariosData["data"]["apellido"]) . "</a></td>";
                        echo "<td>";
                        if($usuariosData['data']['estado'] == 1){ ?>
                            <div class='btn btn-success' data-toggle='tooltip' data-placement='top' title='Validado'><i class='fa fa-check' style="color:white"></i></div>
                            <?php }else{ ?>
                            <div class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Pendiente de validacion'><i class='fas fa-clock' ></i></div>
<?php
                        }
                        echo "<a class='btn btn-warning' data-toggle='tooltip' data-placement='top' title='Ver usuario' href='" . URL_ADMIN . "/index.php?op=ofertas&accion=ver&usuario=" . $data_["usuario"] . "'><i class='fa fa-user'></i></a>";
                        echo "</td>";
                        echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>