<?php
$comentarios = new Clases\ComentariosFB();

if (isset($_GET["leido"]) && isset($_GET["id"])) {
    $comentarios->set("id", $_GET["id"]);
    $comentarios->editSingle("leido", $_GET["leido"]);
}

$comentariosArray = $comentarios->list("", "fecha DESC", "");

if (is_array($comentariosArray)) {
    foreach ($comentariosArray as $key => $comentarioItem) {
        if (empty($comentarioItem["data"]["padre_id"])) {
            $childrens = $comentarios->getChildrens($comentariosArray, $comentarioItem["data"]["id"]);
            !empty($childrens) ? $comentariosArray[$key]["childrens"] = $childrens : null;
        }
    }
    foreach ($comentariosArray as $key => $comentarioItem) {
        if (!empty($comentarioItem["data"]["padre_id"])) {
            unset($comentariosArray[$key]);
        }
    }
}
?>
<div class="mt-20">
    <div class="col-lg-12 col-md-12">
        <h4>
            Comentarios de Facebook
        </h4>
        <hr/>
        <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
        <hr/>
        <?php if (is_array($comentariosArray)) {
            foreach ($comentariosArray as $comentarioItem) {
                $fecha = date("d-m-Y H:i", strtotime($comentarioItem["data"]["fecha"]));
                $leido = $comentarioItem["data"]["leido"]; ?>
                <div class="box-comment">
                    <span class="float-right"><?= $fecha ?></span>
                    <h5><?= $comentarioItem["data"]["nombre"] ?></h5>
                    <span class="fs-14"><?= $comentarioItem["data"]["mensaje"] ?></span><br><br>
                    <?php if ($leido == 1) { ?>
                        <button class="btn btn-success" id="<?= $comentarioItem["data"]["id"] ?>" data-leido="1" onclick="changeStatus('<?= $comentarioItem["data"]["id"] ?>')"><i class="fa fa-eye"></i> Leído</button>
                    <?php } else { ?>
                        <button class="btn btn-primary" id="<?= $comentarioItem["data"]["id"] ?>" data-leido="0" onclick="changeStatus('<?= $comentarioItem["data"]["id"] ?>')"><i class="fa fa-eye"></i> No Leído</button>
                    <?php } ?>
                    <a class="btn btn-info" href="<?= $comentarioItem['data']['url'] ?>" target="_blank"><i class="fa fa-link"></i> Ir al comentario</a>
                    <?php if (isset($comentarioItem["childrens"])) { ?>
                        <a class="btn btn-warning" data-toggle="collapse" href="#collapse-<?= $comentarioItem["data"]["id"] ?>" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-comment"></i> <?= count($comentarioItem["childrens"]) ?> <?= count($comentarioItem["childrens"]) > 1 ? 'Respuestas' : 'Respuesta' ?> (<?= $comentarios->countUnread($comentarioItem["childrens"]) ?> sin leer)
                        </a>
                    <?php } ?>
                </div>
                <?php if (isset($comentarioItem["childrens"])) { ?>
                    <div class="collapse" id="collapse-<?= $comentarioItem["data"]["id"] ?>">
                        <div class="card card-body">
                            <?php foreach ($comentarioItem["childrens"] as $childrenItem) {
                                $fecha = date("d-m-Y H:i", strtotime($childrenItem["data"]["fecha"]));
                                $leido = $childrenItem["data"]["leido"]; ?>
                                <div class="box-comment">
                                    <span class="float-right"><?= $fecha ?></span>
                                    <h5><?= $childrenItem["data"]["nombre"] ?></h5>
                                    <span class="fs-14"><?= $childrenItem["data"]["mensaje"] ?></span><br><br>
                                    <?php if ($leido == 1) { ?>
                                        <button class="btn btn-success" id="<?= $childrenItem["data"]["id"] ?>" data-leido="1" onclick="changeStatus('<?= $childrenItem["data"]["id"] ?>')"><i class="fa fa-eye"></i> Leído</button>
                                    <?php } else { ?>
                                        <button class="btn btn-primary" id="<?= $childrenItem["data"]["id"] ?>" data-leido="0" onclick="changeStatus('<?= $childrenItem["data"]["id"] ?>')"><i class="fa fa-eye"></i> No Leído</button>
                                    <?php } ?>
                                    <a class="btn btn-info" href="<?= $childrenItem['data']['url'] ?>" target="_blank">Ir al comentario</a>
                                </div>
                                <div style="height: 15px"></div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div style="height: 15px"></div>
            <?php }
        } ?>
    </div>
</div>

<script>
    function changeStatus(id) {
        let url = '<?=URL_ADMIN?>';
        let leido;
        if ($('#' + id).attr('data-leido') == 1) {
            leido = 0;
        } else {
            leido = 1;
        }
        console.log(leido);
        $.ajax({
            url: url + "/index.php?op=comentariosfb&accion=ver&id=" + id + "&leido=" + leido,
            type: "GET",
            success: (data) => {
                if (leido == 1) {
                    $('#' + id).attr('class', 'btn btn-success');
                    $('#' + id).attr('data-leido', leido);
                    $('#' + id).html('<i class="fa fa-eye"></i> Leído');
                } else {
                    $('#' + id).attr('class', 'btn btn-primary');
                    $('#' + id).attr('data-leido', leido);
                    $('#' + id).html('<i class="fa fa-eye"></i> No Leído');
                }
            }
        });
    }
</script>