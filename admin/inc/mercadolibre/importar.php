<?php
$config = new Clases\Config();
$meli = $config->viewExportadorMeli();
?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Exportar productos a MercadoLibre
                    <small style="font-size: 30%">v0.2.3</small>
                </h1>
            </div>
            <form class="form-inline text-uppercase mt-10 mb-10" id="formMeli" method="POST" style="margin: auto;" onsubmit="sync()">
                <label class="bold"><input type="checkbox" id="cfg-title" value="1" class="mr-5 ml-10" /> title |</label>
                <label class="bold"><input type="checkbox" id="cfg-price" value="1" class="mr-5 ml-10" /> price |</label>
                <label class="bold"><input type="checkbox" id="cfg-stock" value="1" class="mr-5 ml-10" /> stock |</label>
                <label class="bold"><input type="checkbox" id="cfg-description" value="1" class="mr-5 ml-10" /> description |</label>
                <label class="bold"><input type="checkbox" id="cfg-images" value="1" class="mr-5 ml-10" /> images |</label>
                <input type="hidden" value="<?= $meli["data"]["clasica"] ?>" id="cfg-classic" />
                <input type="hidden" value="<?= $meli["data"]["premium"] ?>" id="cfg-premium" />
                <input type="hidden" value="<?= $meli["data"]["calcular_envio"] ?>" id="cfg-premium" />
                <label class="bold mr-5 ml-10">
                    Tipo de Publicación
                    <select id="type" class=" ml-5 ">
                        <option value="gold_special">Clásica</option>
                        <option value="gold_pro">Premium</option>
                    </select>
                </label>
                <button class="btn btn-success ">SINCRONIZAR</button>
            </form>
        </div>
        <div id="info">

        </div>
    </div>
    <div class="col-md-12 mt-5">
        <div class="row" id="results">
            <table class='table text-center'>
                <thead class='thead-dark'>
                    <tr>
                        <th class='text-center'>CÓDIGO</th>
                        <th>TITULO</th>
                        <th class='text-center'>PRECIO</th>
                        <th class='text-center'>ENVÍO</th>
                        <th class='text-center'>ESTADO</th>
                        <th class='text-center' style="width:400px">MENSAJE</th>
                    </tr>
                </thead>
                <tbody id='resultsRow'>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(".porcentaje").inputSpinner();

    let classic = $('#cfg-classic').val();
    let premium = $('#cfg-premium').val();
    let shipping = $('#cfg-shipping').val();

    function delay() {
        return new Promise(resolve => setTimeout(resolve, 3000));
    }

    function sync() {
        event.preventDefault();
        $('#info').html('');
        $.ajax({
            url: "<?= URL_ADMIN ?>/api/ml/get-products.php",
            type: "POST",
            success: async function(data) {
                data = JSON.parse(data);
                console.log(data);
                if (data['status']) {
                    var total = data['products'].length;
                    $('#info').append("<h5 class='text-center'>Los productos se estan subiendo/actualizando en MercadoLibre, por favor aguarde y no cierre esta página.</h5>");
                    $('#info').append("<progress id='progress-bar' class='prb' max='" + total + "' value='0'></progress>");
                    data['products'].forEach(product => {
                        sendML(product, $('#type').val());
                    });
                }
            }
        });
    }

    function sendML(product, type) {
        const form = $('#formMeli').serialize()
        return $.ajax({
            url: "<?= URL_ADMIN ?>/api/ml/to-meli.php",
            type: "POST",
            data: {
                product: product,
                type: type,
                form: {
                    'cfg-title': $('#cfg-title').is(':checked') ? 1 : 0,
                    'cfg-price': $('#cfg-price').is(':checked') ? 1 : 0,
                    'cfg-stock': $('#cfg-stock').is(':checked') ? 1 : 0,
                    'cfg-description': $('#cfg-description').is(':checked') ? 1 : 0,
                    'cfg-images': $('#cfg-images').is(':checked') ? 1 : 0
                }
            },
            success: function(data) {
                console.log(data)
                data = JSON.parse(data);
                data.forEach((response) => {
                    var error = '';
                    if (!response["status"]) {
                        response["error"].forEach((error_) => {
                            console.log(error_["message"]);
                            error = error_["message"];
                        });
                    }
                    $('#progress-bar').val($('#progress-bar').val() + 1);
                    classTr = (response['data']['status']) ? "bg-success" : "bg-danger";
                    statusIcon = (response['data']['status']) ? "<i class='fa fa-check-square'></i>" : "<i class='fa fa-remove'></i>";
                    $('#resultsRow').append("<tr class='" + classTr + "'><td>" + response["data"]["id"] + "</td><td>" + response["title"] + "</td><td>$" + response["data"]["price"] + "</td><td>$" + response["shipment"] + "</td><td>" + statusIcon + "</td><td>" + error + "</td></tr>");
                });
            }
        });
    }
</script>