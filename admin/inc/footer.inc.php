<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="<?= URL_ADMIN ?>/js/jquery.magicsearch.js"></script>
<script src="<?= URL_ADMIN ?>/js/jQuery.tagify.min.js"></script>

<!-- Tokenfield CSS -->
<link href="<?= URL_ADMIN ?>/js/tags/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">

<script type="text/javascript" src="<?= URL_ADMIN ?>/js/tags/bootstrap-tokenfield.js" charset="UTF-8"></script>

<script src="<?= URL_ADMIN ?>/js/bootstrap-notify.min.js"></script>

<script src="subir-archivos/js/jquery.knob.js"></script>
<script src="subir-archivos/js/jquery.ui.widget.js"></script>
<script src="subir-archivos/js/jquery.iframe-transport.js"></script>
<script src="subir-archivos/js/jquery.fileupload.js"></script>


<?php
echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>';
echo '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>';
echo '<script src="' . URL_ADMIN . '/ckeditor/ckeditor.js"></script>';
echo '<script src="' . URL_ADMIN . '/ckeditor/lang/es.js"></script>';
echo '<script src="'. URL_ADMIN.'/js/script.js"></script>';
?>
<div id="moda-page-ajax" class="modal fade zindex_m">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-uppercase fs-15"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body" id="contenidoForm">
            </div>
        </div>
    </div>
</div>
