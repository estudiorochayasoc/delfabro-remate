<link href="subir-archivos/css/style.css" rel="stylesheet"/>

<a class="btn btn-primary" href="<?=URL_ADMIN?>/index.php?op=subir-archivos&accion=ver-img"><i class="fa fa-cog"></i> Administrar imágenes</a>

<form id="upload" method="post" action="subir-archivos/upload.php" enctype="multipart/form-data">
    <div id="drop">
        Arrastrar imágenes aquí
        <br/>
        <a>Buscar</a>
        <input type="file" name="upl" multiple/>
    </div>

    <ul>

    </ul>

</form>

<script src="subir-archivos/js/script.js"></script>
