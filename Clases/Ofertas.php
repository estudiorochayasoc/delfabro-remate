<?php

namespace Clases;

class ofertas
{

    //Atributos
    public $id;
    public $cod;
    public $rp;
    public $oferta;
    public $producto;
    public $usuario;
    public $estado;
    public $fecha;

    private $con;


    //Metodos
    public function __construct()
    {
        $this->con = new Conexion();;
    }

    public function set($atributo, $valor)
    {
        if (!empty($valor)) {
            $valor = "'" . $valor . "'";
        } else {
            $valor = "NULL";
        }
        $this->$atributo = $valor;
    }

    public function get($atributo)
    {
        return $this->$atributo;
    }

    public function add()
    {
        $sql = "INSERT INTO `ofertas`(`cod` , `rp` , `oferta` , `producto`, `usuario` , `estado` , `fecha`) 
                  VALUES ({$this->cod},
                          {$this->rp},
                          {$this->oferta},
                          {$this->producto},
                          {$this->usuario},
                          {$this->estado},
                          {$this->fecha})";
        $query = $this->con->sqlReturn($sql);
        return $query;
    }

    public function edit()
    {
        $sql = "UPDATE `ofertas` 
                SET cod = {$this->cod},
                    rp = {$this->rp},
                    oferta = {$this->oferta},
                    producto = {$this->producto},
                    usuario = {$this->usuario},
                    estado = {$this->estado},
                    fecha = {$this->fecha} 
                WHERE `cod`={$this->cod}";
        $query = $this->con->sql($sql);
        return $query;
    }

    public function editEstado($valor, $usuario)
    {
        $sql = "UPDATE `ofertas` SET `estado` = {$valor} WHERE `usuario`='{$usuario}'";
        if ($this->con->sqlReturn($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete()
    {
        $sql = "DELETE FROM `ofertas` WHERE `cod`  = {$this->cod}";
        $query = $this->con->sqlReturn($sql);
        if (!empty($this->imagenes->list(array("cod={$this->cod}"), 'orden ASC', ''))) {
            $this->imagenes->cod = $this->cod;
            $this->imagenes->deleteAll();
        }

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    public function view()
    {
        $sql = "SELECT * FROM `ofertas` WHERE rp = {$this->rp} ORDER BY id DESC";
        $ofertas_ = $this->con->sqlReturn($sql);
        $row = mysqli_fetch_assoc($ofertas_);
        return $row;
    }

    public function list($filter = '', $order, $limit, $groupBy = '')
    {
        $array = array();
        if (is_array($filter)) {
            $filterSql = "WHERE ";
            $filterSql .= implode(" AND ", $filter);
        } else {
            $filterSql = '';
        }

        if ($order != '') {
            $orderSql = $order;
        } else {
            $orderSql = "id DESC";
        }

        if ($limit != '') {
            $limitSql = "LIMIT " . $limit;
        } else {
            $limitSql = '';
        }




        $sql = "SELECT * FROM `ofertas` $filterSql $groupBy  ORDER BY $orderSql $limitSql";
        $ofertas = $this->con->sqlReturn($sql);

        if ($ofertas) {
            while ($row = mysqli_fetch_assoc($ofertas)) {
                $array[] = $row;
            }
            return $array;
        }
    }

    public function listGroupBy($filter, $order, $limit)
    {
        $array = array();
        if (is_array($filter)) {
            $filterSql = "WHERE ";
            $filterSql .= implode(" AND ", $filter);
        } else {
            $filterSql = '';
        }

        if ($order != '') {
            $orderSql = $order;
        } else {
            $orderSql = "id DESC";
        }

        if ($limit != '') {
            $limitSql = "LIMIT " . $limit;
        } else {
            $limitSql = '';
        }

        $sql = "SELECT * FROM `ofertas` $filterSql GROUP BY producto ORDER BY $orderSql   $limitSql";
        $ofertas = $this->con->sqlReturn($sql);

        if ($ofertas) {
            while ($row = mysqli_fetch_assoc($ofertas)) {
                $array[] = $row;
            }
            return $array;
        }
    }


    public function listGroupByUser($filter, $order, $limit)
    {
        $array = array();
        if (is_array($filter)) {
            $filterSql = "WHERE ";
            $filterSql .= implode(" AND ", $filter);
        } else {
            $filterSql = '';
        }

        if ($order != '') {
            $orderSql = $order;
        } else {
            $orderSql = "id DESC";
        }

        if ($limit != '') {
            $limitSql = "LIMIT " . $limit;
        } else {
            $limitSql = '';
        }

        $sql = "SELECT * FROM `ofertas` $filterSql GROUP BY usuario ORDER BY $orderSql   $limitSql";
        echo $sql;
        $ofertas = $this->con->sqlReturn($sql);

        if ($ofertas) {
            while ($row = mysqli_fetch_assoc($ofertas)) {
                $array[] = $row;
            }
            return $array;
        }
    }


    public function strposa($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
                $res = strpos($haystack, $needle, $offset);
                if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
}

}
