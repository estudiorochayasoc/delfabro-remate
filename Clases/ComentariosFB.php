<?php

namespace Clases;

class ComentariosFB
{

    //Atributos
    public $id;
    public $nombre;
    public $mensaje;
    public $url;
    public $padreId;
    public $leido = 0;
    public $fecha;
    private $con;

    //Metodos
    public function __construct()
    {
        $this->con = new Conexion();
    }

    public function set($atributo, $valor)
    {
        if (!empty($valor)) {
            $valor = "'" . $valor . "'";
        } else {
            $valor = "NULL";
        }
        $this->$atributo = $valor;
    }

    public function get($atributo)
    {
        return $this->$atributo;
    }

    public function add()
    {
        $sql = "INSERT INTO `comentarios_fb`(`id`, `nombre`, `mensaje`, `url`, `padre_id`, `leido`,`fecha`) 
                VALUES ({$this->id},
                        {$this->nombre},
                        {$this->mensaje},
                        {$this->url},
                        {$this->padreId},
                        {$this->leido},
                        {$this->fecha})";
        $query = $this->con->sql($sql);
        return $query;
    }

    public function edit()
    {
        $sql = "UPDATE `comentarios_fb` 
                SET nombre = {$this->nombre},
                    mensaje = {$this->mensaje},
                    url = {$this->url},
                    padre_id = {$this->padreId},
                    leido = {$this->leido},
                    fecha = {$this->fecha} 
                WHERE `id`={$this->id}";
        $query = $this->con->sql($sql);
        return $query;
    }

    public function editSingle($atributo, $valor)
    {
        $sql = "UPDATE `comentarios_fb` SET `$atributo` = {$valor} WHERE `id`={$this->id}";
        $this->con->sql($sql);
    }

    public function delete()
    {
        $sql = "DELETE FROM `comentarios_fb` WHERE `id`  = {$this->id}";
        $query = $this->con->sqlReturn($sql);

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    public function getChildrens($comentariosArray, $padreId)
    {
        $childrens = [];

        foreach ($comentariosArray as $comentarioItem) {
            if ($padreId == $comentarioItem["data"]["padre_id"]) {
                $childrens[] = $comentarioItem;
            }
        }

        return $childrens;
    }

    public function countUnread($comentariosArray)
    {
        $count = 0;

        foreach ($comentariosArray as $comentarioItem) {
            if ($comentarioItem["data"]["leido"] == 0) {
                $count++;
            }
        }

        return $count;
    }

    public function view()
    {
        $sql = "SELECT * FROM `comentarios_fb` WHERE id = {$this->id} ORDER BY id DESC LIMIT 1";
        $comentario = $this->con->sqlReturn($sql);
        $row = mysqli_fetch_assoc($comentario);
        $array = array("data" => $row);
        return $array;
    }

    function list($filter, $order, $limit)
    {
        $array = array();
        if (is_array($filter)) {
            $filterSql = "WHERE ";
            $filterSql .= implode(" AND ", $filter);
        } else {
            $filterSql = '';
        }

        if ($order != '') {
            $orderSql = $order;
        } else {
            $orderSql = "id DESC";
        }

        if ($limit != '') {
            $limitSql = "LIMIT " . $limit;
        } else {
            $limitSql = '';
        }

        $sql = "SELECT * FROM `comentarios_fb` $filterSql ORDER BY $orderSql $limitSql";
        $comentario = $this->con->sqlReturn($sql);
        if ($comentario) {
            while ($row = mysqli_fetch_assoc($comentario)) {
                $array[] = array("data" => $row);
            }
            return $array;
        }
    }

}
