<?php
require_once "Config/Autoload.php";
Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$novedades = new Clases\Novedades();
$categoria = new Clases\Categorias();

#Variables GET
$cod = $f->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');

#View de la novedad actual
$novedades->set("cod", $cod);
$novedadData = $novedades->view();

#Redireccionar si no existe la novedad actual
empty($novedadData['data']) ? $f->headerMove(URL . '/novedades') : null;

#List de últimas novedades
$novedadesArraySide = $novedades->list("", "", 8);
#List de categorias
$categoriasArray = $categoria->list(["area = 'novedades'"], "", "");

#Fecha normalizada de la novedad actual
$fecha = strftime("%u de %B de %Y", strtotime($novedadData['data']['fecha']));

#Información de cabecera
$template->set("title", ucfirst(mb_strtolower($novedadData['data']['titulo'])) . " | " . TITULO);
$template->set("description", mb_substr(strip_tags($novedadData['data']['desarrollo'], '<p><a>'), 0, 160));
$template->set("keywords", strip_tags($novedadData['data']['keywords']));
$template->set("imagen", isset($novedadData['images'][0]['ruta']) ? URL . '/' . $novedadData['images'][0]['ruta'] : LOGO);
$template->themeInit();
?>

<!-- start blog-with-sidebar -->
<section class="blog-single section-padding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 col-md-9 blog-single-content">
                <div class="post">
                    <div class="post-title-meta pt-40">
                        <h2><?= $novedadData['data']['titulo'] ?></h2>
                        <ul>
                            <li><a href="#"><?= $fecha ?></a></li>
                        </ul>
                    </div>
                    <div class="media">
                        <?php if (!empty($novedadData['images'])) { ?>
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php foreach ($novedadData['images'] as $key => $img) { ?>
                                        <div class="item <?= ($key == 0) ? "active" : null ?>">
                                            <div style="height:500px;background:url(<?= URL ?>/<?= $img['ruta'] ?>)center/cover;"></div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if (count($novedadData['images']) > 1) { ?>
                                    <a class="left carousel-control" href="#carouselExampleControls" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Anterior</span>
                                    </a>
                                    <a class="right carousel-control" href="#carouselExampleControls" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Siguiente</span>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="post-body">
                        <?= $novedadData['data']['desarrollo'] ?>
                    </div>
                </div> <!-- end post -->

                <div class="tag-share">
                    <div>
                        <span>Tags: </span>
                        <ul class="tag">
                            <?php foreach (explode(",", $novedadData['data']['keywords']) as $tag) { ?>
                                <li><a href="#"><?= $tag ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div>
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_email"></a>
                            <a class="a2a_button_google_gmail"></a>
                            <a class="a2a_button_whatsapp"></a>
                        </div>
                        <script async src="https://static.addtoany.com/menu/page.js"></script>
                    </div>
                </div>

                <div>
                    <div class="fb-comments" data-href="<?= URL . '/novedad/' . $f->normalizar_link($novedadData['data']['titulo']) . '/' . $novedadData['data']['cod'] ?>" data-width="100%" data-numposts="5" data-order-by="social"></div>
                </div>
            </div> <!-- end blog-content -->

            <div class="blog-sidebar col col-lg-3 col-lg-offset-1 col-md-3 col-sm-5">
                <div class="widget search-widget">
                    <form class="form" method="get" action="<?= URL ?>/novedades">
                        <input oninvalid="this.setCustomValidity('Únicamente se permiten letras y números.')" oninput="this.setCustomValidity('')" type="text" name="buscar" pattern="[^()/><\][\\\x22#'´`¨{}^*,;.|!¡?¿$%&]+" class="form-control" placeholder="Buscar..">
                    </form>
                </div>
                <div class="widget category-widget">
                    <h3>Categorías</h3>
                    <ul>
                        <?php foreach ($categoriasArray as $categoriaItem) { ?>
                            <li>
                                <a href="<?= URL . '/novedades/' . $f->normalizar_link($categoriaItem["data"]["titulo"]) ?>">
                                    <?= $categoriaItem["data"]["titulo"] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="widget recent-post-widget">
                    <h3>Recientes</h3>
                    <ul>
                        <?php
                        foreach ($novedadesArraySide as $novedadItem) {
                            $img = isset($novedadItem['images'][0]['ruta']) ? $novedadItem['images'][0]['ruta'] : 'assets/archivos/sin_imagen.jpg';
                            $img = URL . '/' . $img;
                            $fecha = strftime("%u de %B de %Y", strtotime($novedadItem['data']['fecha']));
                            $link = URL . '/novedad/' . $f->normalizar_link($novedadItem['data']["titulo"]) . '/' . $novedadItem['data']['cod'];
                        ?>
                            <li>
                                <div class="post-pic" style="background: url(<?= $img ?>)center/cover no-repeat; height: 67px;"></div>
                                <div class="details">
                                    <span><?= $fecha ?></span>
                                    <h4 class="title-blog-side-overflow"><a href="<?= $link ?>"><?= mb_substr($novedadItem['data']['titulo'], 0, 60); ?></a></h4>
                                </div>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!-- end blog-with-section -->

<?php $template->themeEnd() ?>