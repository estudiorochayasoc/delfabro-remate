<?php
require_once "Config/Autoload.php";
Config\Autoload::run();
$template = new Clases\TemplateSite();

$f = new Clases\PublicFunction();
$checkout = new Clases\Checkout();
$carrito = new Clases\Carrito();
$config = new Clases\Config();

#Se obtiene la key del captcha
$captchaData = $config->viewCaptcha();

#Redireccionar al carrito si no se abrieron los stages (en carrito se abren los stages)
if (empty($_SESSION['stages'])) {
    $f->headerMove(URL . '/carrito');
} else {
    #Revisar si hay un usuario ya sea invitado o no
    if (!empty($_SESSION['usuarios'])) {
        #Si por alguna razón no se guardo el user_cod, se guarda
        if (empty($_SESSION['stages']['user_cod'])) {
            $checkout->user($_SESSION['usuarios']['cod'], 'USER');
            #Si ya tiene guardado el user_cod, se redirecciona a shipping
        } else {
            $f->headerMove(URL . '/checkout/shipping');
        }
    }
}

#Variable que almacena el progeso de los stages
$progress = $checkout->progress();

#Información de cabecera
$template->set("title", 'Identificación | ' . TITULO);
$template->themeInitStages();
?>
<div class="checkout-estudiorocha">
    <?php
    if (!empty($_SESSION['stages'])) {

        if (empty($_SESSION['stages']['user_cod'])) {
    ?>
            <div class="container mt-40">
                <div class="row">
                    <div class="col-md-4 col-sm-4 ">
                        <div class="box mb-40">
                            <div class="text-center">
                                Si estás interesado en comprar sin necesidad de crear una cuenta, hacé click en el siguiente botón.
                                <br />
                                <i class="fa fa-arrow-down"></i>
                                <br />
                                <a href="<?= URL ?>/checkout/shipping" class="btn btn-primary btn-lg btn-block">COMPRAR COMO INVITADO</a>
                            </div>
                        </div>
                        <div class="box mb-40">
                            <h2 class="fs-25  text-uppercase">Ingresar</h2>
                            <hr />
                            <div id="l-error"></div>
                            <form id="login" data-url="<?= URL ?>" data-type="stages" onsubmit="loginUser()">
                                <input class="form-control" type="hidden" name="stg-l" value="1">
                                <div class="form-fild">
                                    <span><label>Email <span class="required">*</span></label></span>
                                    <input class="form-control" name="l-user" value="" type="email" required>
                                </div>
                                <div class="form-fild">
                                    <span><label>Contraseña <span class="required">*</span></label></span>
                                    <input class="form-control" name="l-pass" id="l-pass" value="" type="password" required>
                                </div>
                                <div class="form-fild mt-15">
                                    <div id="RecaptchaField1"></div>
                                </div>
                                <div id="btn-l" class="login-submit mt-10 mb-10">
                                    <input type="submit" value="INGRESAR" id="ingresar" class="btn btn-success">
                                </div>
                                <div class="lost-password">
                                    <a href="<?= URL ?>/recuperar">Olvidaste tu contraseña? Haz click aquí.</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <div class="box mb-40">
                            <h2 class="fs-25  text-uppercase">Registrarse</h2>
                            <hr />
                            <div id="r-error"></div>
                            <form id="register" data-url="<?= URL ?>" data-type="stages" onsubmit="registerUser()">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-fild">
                                            <span><label>Nombre <span class="required">*</span></label></span>
                                            <input class="form-control" name="r-nombre" value="" type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-fild">
                                            <span><label>Apellido <span class="required">*</span></label></span>
                                            <input class="form-control" name="r-apellido" value="" type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-fild">
                                            <span><label>Email <span class="required">*</span></label></span>
                                            <input class="form-control" name="r-email" value="" type="email" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-fild">
                                            <span><label>Contraseña <span class="required">*</span></label></span>
                                            <input class="form-control" name="r-password1" value="" type="password" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-fild">
                                            <span><label>Repetir contraseña <span class="required">*</span></label></span>
                                            <input class="form-control" name="r-password2" value="" type="password" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-fild">
                                            <span><label>Dirección <span class="required">*</span></label></span>
                                            <input class="form-control" name="r-direccion" value="" type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-fild">
                                            <span><label>Teléfono <span class="required">*</span></label></span>
                                            <input class="form-control" name="r-telefono" value="" type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-fild">
                                            <span><label>Provincia <span class="required">*</span></label></span>
                                            <select id='provincia' data-url="<?= URL ?>" class="form-control" name="provincia" required>
                                                <option value="" selected>Seleccionar Provincia</option>
                                                <?php $f->provincias(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-fild">
                                            <span><label>Localidad <span class="required">*</span></label></span>
                                            <select id='localidad' class="form-control" name="localidad" required>
                                                <option value="" selected>Seleccionar Localidad</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-fild mt-15">
                                    <div id="RecaptchaField2"></div>
                                </div>
                                <div id="btn-r" class="register-submit mt-10 mb-10">
                                    <input type="submit" value="Registrar" id="registrar" class="btn btn-success">
                                </div>
                            </form>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
    <?php
        } else {
            $f->headerMove(URL . '/checkout/shipping');
        }
    } else {
        $f->headerMove(URL . '/carrito');
    }
    ?>
</div>
<?php
$template->themeEndStages();
?>

<script src="<?= URL ?>/assets/js/services/user.js"></script>

<script>
    CaptchaCallback('RecaptchaField1', '<?= $captchaData['data']['captcha_key'] ?>');
    CaptchaCallback('RecaptchaField2', '<?= $captchaData['data']['captcha_key'] ?>');
</script>