<?php
require_once "Config/Autoload.php";
Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$contenido = new Clases\Contenidos();
$config = new Clases\Config();
$usuario = new Clases\Usuarios();
$enviar = new Clases\Email();

#Se carga la sesión del usuario
$userData = $usuario->viewSession();

#Comprueba si existe la sesión
if (!empty($userData)) {
    #Si existe y es un usuario registrado, lo redirige a su panel
    if ($userData["invitado"] == 0) {
        $f->headerMove(URL . '/sesion');
    }
    #Si existe y es un usuario invitado, lo redirige a usuarios para loguearse o registrarse
    if ($userData["invitado"] == 1) {
        $usuario->logout();
        $f->headerMove(URL . '/usuarios');
    }
}


$getReturnURL = isset($_GET["getReturnURL"]) ? $_GET["getReturnURL"] : '';

#Se carga la configuración de email
$emailData = $config->viewEmail();

#Se carga la key del captcha
$captchaData = $config->viewCaptcha();

#Información de cabecera
$template->set("title", 'Acceso de usuarios | ' . TITULO);
$template->set("description", "Accedé con tu cuenta o registráte para empezar a comprar en nuestra tienda online.");
$template->set("keywords", "acceso de usuarios, login, registrarse, usuarios");
$template->themeInit();
?>

<div class="pa-breadcrumb container-fluid" style="background:  url(<?= URL . "/" . "assets/images/header.jpg" ?>)"></div>


<div id="content" class="site-content mt-50 mb-50" tabindex="-1">
    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 ">
                    <div class="box">
                        <h2 style="color: #282828;border-bottom:3px solid #F1C40F">INGRESAR</h2>
                        <div id="l-error"></div>
                        <form id="login" data-url="<?= URL ?>" data-type="usuarios" onsubmit="loginUser('<?= $getReturnURL ?>')">
                            <input class="form-control" type="hidden" name="stg-l" value="1">
                            <div class="form-fild mb-10">
                                <span><label>Email <span class="required">*</span></label></span>
                                <input class="form-control" name="l-user" value="" type="email" required>
                            </div>
                            <div class="form-fild mb-10">
                                <span><label>Contraseña <span class="required">*</span></label></span>
                                <input class="form-control" name="l-pass" id="l-pass" value="" type="password" required>
                            </div>
                            <div class="form-fild mt-15">
                                <div id="RecaptchaField1"></div>
                            </div>
                            <div id="btn-l" class="login-submit mt-10 mb-10">
                                <input type="submit" value="INGRESAR" id="ingresar" class="btn btn-custom">
                            </div>
                            <div class="lost-password">
                                <a href="<?= URL ?>/recuperar">¿Olvidaste tu contraseña? Hacé click acá.</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="box">
                        <h2 style="color: #282828;border-bottom:3px solid #F1C40F">REGISTRARSE</h2>
                        <div id="r-error"></div>
                        <p>Para participar del sistema de pre-oferta debe completar los siguientes datos, luego van a confirmar sus datos para habilitar su cuenta. Muchas gracias.</p>
                        <form id="register" data-url="<?= URL ?>" data-type="usuarios" onsubmit="registerUser()">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-fild mb-10">
                                        <span><label>Nombre <span class="required">*</span></label></span>
                                        <input class="form-control" name="r-nombre" value="" type="text" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-fild mb-10">
                                        <span><label>Apellido <span class="required">*</span></label></span>
                                        <input class="form-control" name="r-apellido" value="" type="text" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-fild mb-10">
                                        <span><label>Email <span class="required">*</span></label></span>
                                        <input class="form-control" name="r-email" value="" type="email" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-fild mb-10">
                                        <span><label>Contraseña <span class="required">*</span></label></span>
                                        <input class="form-control" name="r-password1" value="" type="password" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-fild mb-10">
                                        <span><label>Repetir contraseña <span class="required">*</span></label></span>
                                        <input class="form-control" name="r-password2" value="" type="password" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-fild mb-10">
                                        <span><label>Dirección <span class="required">*</span></label></span>
                                        <input class="form-control" name="r-direccion" value="" type="text" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-fild mb-10">
                                        <span><label>Teléfono <span class="required">*</span></label></span>
                                        <input class="form-control" name="r-telefono" value="" type="text" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-fild mb-10">
                                        <span><label>Provincia <span class="required">*</span></label></span>
                                        <select id='provincia' data-url="<?= URL ?>" class="form-control" name="provincia" required>
                                            <option value="" selected>Seleccionar Provincia</option>
                                            <?php $f->provincias(); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-fild mb-10">
                                        <span><label>Localidad <span class="required">*</span></label></span>
                                        <select id='localidad' class="form-control" name="localidad" required>
                                            <option value="" selected>Seleccionar Localidad</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-fild mt-15">
                                <div id="RecaptchaField2"></div>
                            </div>
                            <div id="btn-r" class="register-submit mt-10 mb-10">
                                <input type="submit" value="REGISTRAR MI CUENTA" id="registrar" class="btn btn-custom fs-18 bold">
                            </div>
                        </form>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Login Register section end-->
<?php $template->themeEnd(); ?>
<script src="<?= URL ?>/assets/js/services/user.js"></script>
<script>
    function captchaTimer() {
        try {
            CaptchaCallback('RecaptchaField1', '<?= $captchaData['data']['captcha_key'] ?>');
            CaptchaCallback('RecaptchaField2', '<?= $captchaData['data']['captcha_key'] ?>');
        } catch (err) {
            setTimeout(captchaTimer, 1000);
        }
    }

    captchaTimer();
</script>