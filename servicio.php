<?php
require_once "Config/Autoload.php";
Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$servicios = new Clases\Servicios();
$categoria = new Clases\Categorias();

#Variables GET
$cod = $f->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');

#View del servicio actual
$servicios->set("cod", $cod);
$servicioData = $servicios->view();

#Redireccionar si no existe el servicio actual
empty($servicioData['data']) ? $f->headerMove(URL . '/servicios') : null;

#List de últimos servicios
$serviciosArraySide = $servicios->list("", "", 8);
#List de categorias
$categoriasArray = $categoria->list(["area = 'servicios'"], "", "");

#Fecha normalizada del servicio actual
$fecha = strftime("%u de %B de %Y", strtotime($servicioData['data']['fecha']));

#Información de cabecera
$template->set("title", ucfirst(mb_strtolower($servicioData['data']['titulo'])) . " | " . TITULO);
$template->set("description", mb_substr(strip_tags($servicioData['data']['desarrollo'], '<p><a>'), 0, 160));
$template->set("keywords", strip_tags($servicioData['data']['keywords']));
$template->set("imagen", isset($servicioData['images'][0]['ruta']) ? URL . '/' . $servicioData['images'][0]['ruta'] : LOGO);
$template->themeInit();
?>

<!-- start blog-with-sidebar -->
<section class="blog-single section-padding pt-60">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 blog-single-content">
                <div class="post">
                    <div class="post-title-meta pt-40">
                        <h2><?= $servicioData['data']['titulo'] ?></h2>
                    </div>
                    <div class="media">
                        <?php if (!empty($servicioData['images'])) { ?>
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php foreach ($servicioData['images'] as $key => $img) { ?>
                                        <div class="item <?= ($key == 0) ? "active" : null ?>">
                                            <div style="height:500px;background:url(<?= URL ?>/<?= $img['ruta'] ?>)center/cover;"></div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if (count($servicioData['images']) > 1) { ?>
                                    <a class="left carousel-control" href="#carouselExampleControls" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Anterior</span>
                                    </a>
                                    <a class="right carousel-control" href="#carouselExampleControls" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Siguiente</span>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="post-body">
                        <?= $servicioData['data']['desarrollo'] ?>
                    </div>
                </div> <!-- end post -->

                <div class="tag-share">
                    <div>
                        <span>Tags: </span>
                        <ul class="tag">
                            <?php foreach (explode(",", $servicioData['data']['keywords']) as $tag) { ?>
                                <li><a href="#"><?= $tag ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div>
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_email"></a>
                            <a class="a2a_button_google_gmail"></a>
                            <a class="a2a_button_whatsapp"></a>
                        </div>
                        <script async src="https://static.addtoany.com/menu/page.js"></script>
                    </div>
                </div>

                <div>
                    <div class="fb-comments" data-href="<?= URL . '/servicio/' . $f->normalizar_link($servicioData['data']['titulo']) . '/' . $servicioData['data']['cod'] ?>" data-width="100%" data-numposts="5" data-order-by="social"></div>
                </div>
            </div> <!-- end blog-content -->

        </div>
    </div> <!-- end container -->
</section>
<!-- end blog-with-section -->

<?php $template->themeEnd() ?>