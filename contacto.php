<?php
require_once "Config/Autoload.php";
Config\Autoload::run();

$template = new Clases\TemplateSite();
$f = new Clases\PublicFunction();
$contenidos = new Clases\Contenidos();
$config = new Clases\Config();
$enviar = new Clases\Email();

#Se carga la key del captcha
$captchaData = $config->viewCaptcha();

#Se carga la configuración de contacto
$contactData = $config->viewContact();

#Se carga la configuración de email
$emailData = $config->viewEmail();

#Información de cabecera
$template->set("title", "Contacto | " . TITULO);
$template->set("description", "Envianos tus dudas y nosotros te asesoramos");
$template->set("keywords", "contactar verduleria san francisco, san francisco córdoba contactar verduleria, frutas y verduras san francisco,contactar verduleria online");
$template->themeInit();
?>
<!-- start page-title -->
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12">
                <h2>Contacto</h2>
                <ol class="breadcrumb">
                    <li><a href="<?= URL ?>">Inicio</a></li>
                    <li>Contacto</li>
                </ol>
            </div>
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>
<!-- end page-title -->

<!-- start contact-pg-section -->
<section class="contact-pg-section section-padding">
    <div class="container">
        <?= $contenidos->list(["titulo = 'DATOS DE CONTACTO'"], "", "")[0]["data"]["contenido"] ?>
        <div class="row">
            <div class="col col-xs-12">
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
                <script>
                    hbspt.forms.create({
                        portalId: "8333647",
                        formId: "5ce64416-b4b9-43a5-a2af-0ead8c53c1a7"
                    });
                </script>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!-- end contact-pg-section -->

<?php $template->themeEnd() ?>