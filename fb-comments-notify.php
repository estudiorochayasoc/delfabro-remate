<?php
require_once dirname(__DIR__, 2) . "/Config/Autoload.php";

Config\Autoload::run();

$comentario = new Clases\ComentariosFB();
$enviar = new Clases\Email();
$config = new Clases\Config();

$emailData = $config->viewEmail();
$marketingData = $config->viewMarketing();

//echo ($_GET['hub_verify_token'] === '123456') ? $_GET['hub_challenge'] : null;

$request = json_decode(file_get_contents('php://input'));
$urlComment = json_decode(file_get_contents('https://graph.facebook.com/v3.1/' . $request->entry[0]->changes[0]->value->id . '?access_token=' . $marketingData["data"]["facebook_access_token"] . '&fields=permalink_url'));

$comentario->set("id", $request->entry[0]->changes[0]->value->id);
$comentario->set("nombre", $request->entry[0]->changes[0]->value->from->name);
$comentario->set("mensaje", $request->entry[0]->changes[0]->value->message);
$comentario->set("url", $urlComment->permalink_url);
$comentario->set("padreId", isset($request->entry[0]->changes[0]->value->parent->id) ? $request->entry[0]->changes[0]->value->parent->id : null);
$comentario->set("fecha", $request->entry[0]->changes[0]->value->created_time);
$comentario->add();

//$enviar->set("asunto", "fb");
//$enviar->set("receptor", "webestudiorocha@gmail.com");
//$enviar->set("emisor", $emailData['data']['remitente']);
//$enviar->set("mensaje", $req);
//$enviar->emailEnviar();
