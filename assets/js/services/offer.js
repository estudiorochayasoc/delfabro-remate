
function refreshPrice(url, cod, rp) {
  $.ajax({
    url: url + "/api/offer/offerRefresh.php?cod=" + cod + "&rp=" + rp,
    type: "GET",
    success: function (data) {
      console.log(data);
      data = JSON.parse(data);
      data.forEach(data_ => {
        if (data_.price != 0) {
          $('#refreshPrice' + data_.rp).html("$" + data_.price);
        }
      });
    }
  });
}