<?php
$config = new Clases\Config();

#Se carga la configuración de marketing
$marketing = $config->viewMarketing();

#Se carga la configuración del header y se la muestra
$configHeader = $config->viewConfigHeader();
echo $configHeader["data"]["content_header"];

#Script One Signal
if (!empty($marketing['data']['onesignal'])) { ?>
    <link rel="manifest" href="/manifest.json" />
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "<?= $marketing["data"]["onesignal"] ?>",
            });
        });
    </script>
<?php }

#Script Google Analytics
if (!empty($marketing['data']['google_analytics'])) { ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-150839106-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', '<?= $marketing["data"]["google_analytics"] ?>');
    </script>
<?php }

#Script Pixel Facebook
if (!empty($marketing['data']['facebook_pixel'])) { ?>
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '<?= $marketing['data']['facebook_pixel'] ?>');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=<?= $marketing['data']['facebook_pixel'] ?>&ev=PageView
&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->

<?php }
if (!empty($marketing['data']['facebook_app_comment'])) { ?>
    <div id="fb-root"></div>

    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: '<?= $marketing["data"]["facebook_app_comment"] ?>',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v5.0'
            });
        };
    </script>
    <script async defer src="https://connect.facebook.net/es_ES/sdk.js"></script>
<?php }

#Script Hubspot
if (!empty($marketing['data']['hubspot'])) { ?>
    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/<?= $marketing['data']['hubspot'] ?>.js"></script>
    <!-- End of HubSpot Embed Code -->
<?php } ?>

<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">


<!-- Icon fonts -->
<link href="<?= URL ?>/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/animate.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/owl.carousel.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/owl.theme.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/slick.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/slick-theme.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/owl.transitions.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/style.css" rel="stylesheet">


<!-- Default Styles -->
<meta property="fb:app_id" content="<?= isset($marketing["data"]["facebook_app_comment"]) ? strip_tags($marketing["data"]["facebook_app_comment"]) : '' ?>" />

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<link rel="stylesheet" href="<?= URL ?>/assets/css/main-rocha.css">
<link rel="stylesheet" href="<?= URL ?>/assets/css/estilos-rocha.css">
<link rel="stylesheet" href="<?= URL ?>/assets/css/select2.min.css">
<link rel="stylesheet" href="<?= URL ?>/assets/css/loading.css">
<link href="<?= URL ?>/assets/css/progress-wizard.min.css" rel="stylesheet">