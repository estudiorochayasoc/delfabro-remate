<?php
//Clases
$hub = new Clases\Hubspot();
$usuario->set("cod", $_SESSION["usuarios"]["cod"]);
$usuarioData = $usuario->view();
?>
<div class="col-md-12 mb-10">
    <?php
    if (isset($_POST["guardar"])):
        $pass = false;
        $error = '';

        $nombre = $f->antihack_mysqli(!empty($_POST["nombre"]) ? $_POST["nombre"] : '');
        $apellido = $f->antihack_mysqli(!empty($_POST["apellido"]) ? $_POST["apellido"] : '');
        $email = $f->antihack_mysqli(!empty($_POST["email"]) ? $_POST["email"] : '');
        $password = $f->antihack_mysqli(!empty($_POST["password"]) ? $_POST["password"] : '');
        $provincia = $f->antihack_mysqli(!empty($_POST["provincia"]) ? $_POST["provincia"] : '');
        $localidad = $f->antihack_mysqli(!empty($_POST["localidad"]) ? $_POST["localidad"] : '');
        $direccion = $f->antihack_mysqli(!empty($_POST["direccion"]) ? $_POST["direccion"] : '');
        $telefono = $f->antihack_mysqli(!empty($_POST["telefono"]) ? $_POST["telefono"] : '');
        $celular = $f->antihack_mysqli(!empty($_POST["celular"]) ? $_POST["celular"] : '');
        $postal = $f->antihack_mysqli(!empty($_POST["postal"]) ? $_POST["postal"] : '');

        if (!empty($_POST["password"]) && !empty($_POST["password2"])) {
            if ($_POST["password"] == $_POST['password2']) {
                $pass = true;
                $password = $f->antihack_mysqli($_POST["password"]);
            } else {
                $error = '<div class="alert alert-warning" role="alert">Las contraseña no coinciden</div>';
            }
        }

        if (empty($error)) {
            $usuario->set("cod", $usuarioData['data']['cod']);
            $usuario->set("email", $email);

            $usuario->editSingle("nombre", $nombre);
            $usuario->editSingle("apellido", $apellido);
            $usuario->editSingle("email", $email);
            $usuario->editSingle("provincia", $provincia);
            $usuario->editSingle("localidad", $localidad);
            $usuario->editSingle("direccion", $direccion);
            $usuario->editSingle("telefono", $telefono);
            $usuario->editSingle("celular", $celular);
            $usuario->editSingle("postal", $postal);
            if ($pass) $usuario->editSingle("password", $password);
            $usuario->editSingle("fecha", $usuarioData['data']['fecha']);
            $f->headerMove(URL . '/sesion/cuenta');
        }
    endif;
    ?>
    <br>
    <form class="login_form" id="registro" method="post" autocomplete="off">
        <div class="row">
            <?= !empty($error) ? $error : '' ?>
            <div class="col-md-6">Nombre
                <div class="input-group">
                    <input class="form-control h40"
                           value="<?= $usuarioData['data']['nombre'] ?>"
                           type="text"
                           placeholder="Nombre"
                           name="nombre"
                           required/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-user"></i></span>
                </div>
            </div>
            <div class="col-md-6">Apellido
                <div class="input-group">
                    <input class="form-control h40"
                           value="<?= $usuarioData['data']['apellido'] ?>"
                           type="text"
                           placeholder="Apellido"
                           name="apellido"
                           required/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-user"></i></span>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">Email
                <div class="input-group">
                    <input class="form-control h40"
                           value="<?= $usuarioData['data']['email'] ?>"
                           type="email"
                           placeholder="Email"
                           name="email"
                           required/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-envelope"></i></span>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">Teléfono
                <div class="input-group">
                    <input class="form-control h40"
                           value="<?= $usuarioData['data']['telefono'] ?>"
                           type="number"
                           placeholder="Teléfono"
                           name="telefono"
                           required/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-phone"></i></span>
                </div>
            </div>
            <div class="col-md-6">Celular
                <div class="input-group">
                    <input class="form-control h40"
                           value="<?= $usuarioData['data']['celular'] ?>"
                           type="number"
                           placeholder="Celular"
                           name="celular"
                           required/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-phone"></i></span>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">Provincia
                <div class="input-group">
                    <select class="pull-right form-control h40" name="provincia" id="provincia" required>
                        <option value="<?= $usuarioData['data']['provincia'] ?>" selected><?= $usuarioData['data']['provincia'] ?></option>
                        <option value="" disabled>Provincia</option>
                        <?php $f->provincias() ?>
                    </select>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-map-marker"></i></span>
                </div>
            </div>
            <div class="col-md-6">Localidad
                <div class="input-group">
                    <select class="form-control h40" name="localidad" id="localidad" required>
                        <option value="<?= $usuarioData['data']['localidad'] ?>"
                                selected><?= $usuarioData['data']['localidad'] ?></option>
                        <option value="" disabled>Localidad</option>
                    </select>
                    <span class="input-group-addon"><i
                                class="login_icon glyphicon glyphicon-map-marker"></i></span>
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">Dirección
                <div class="input-group">
                    <input class="form-control h40"
                           value="<?= $usuarioData['data']['direccion'] ?>"
                           type="text"
                           placeholder="Dirección"
                           name="direccion"
                           required/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-map-marker"></i></span>
                </div>
            </div>
            <div class="col-md-6">Código Postal
                <div class="input-group">
                    <input class="form-control h40"
                           value="<?= $usuarioData['data']['postal'] ?>"
                           type="text"
                           placeholder="Código Postal"
                           name="postal"
                           required/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-map-marker"></i></span>
                </div>
            </div>
        </div>
        <br/>
        <hr>
        <sup>*Dejar vacío si no se desea cambiar la contraseña</sup>
        <br>
        <div class="row">
            <div class="col-md-6">Contraseña
                <input type="password" name="password" id="password_fake" class="hidden" autocomplete="off" style="display: none;">
                <div class="input-group">
                    <input autocomplete="off" class="form-control h40" value="" type="password" placeholder="Contraseña" name="password"/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-lock"></i></span>
                </div>
            </div>
            <div class="col-md-6">Confirmar Contraseña
                <div class="input-group">
                    <input class="form-control h40" value="" type="password" placeholder="Confirmar Contraseña" name="password2"/>
                    <span class="input-group-addon"><i class="login_icon glyphicon glyphicon-lock"></i></span>
                </div>
            </div>
        </div>
        <br/>
        <button style="width: 100%;" type="submit" name="guardar" class="btn btn-success">Guardar</button>
    </form>
    <br>
</div>