<?php
//Clases
$ofertas = new Clases\Ofertas();
$producto = new Clases\Productos();
$f = new Clases\PublicFunction();
$ofertasData = $ofertas->listGroupBy(["usuario = '" . $_SESSION['usuarios']['cod'] . "'"], 'fecha DESC', '');

if (empty($ofertasData)) {
?>
    <div class="container centro">
        <h4>No hay ofertas todavía.</h4>
    </div>
<?php
} else {
?>
    <div class="col-md-12 mb-10 pedidos" style="margin-top:10px;">
        <?php
        foreach ($ofertasData as $value) {
            $producto->set("cod", $value["producto"]);
            $productoData = $producto->view();
        ?>
            <div class="panel panel-warning mt-10">
                <div class="panel panel-heading" role="tab" id="heading" style="background-color: #000;">
                    <a data-toggle="collapse" href="#collapse<?= $value["cod"] ?>" aria-expanded="false" aria-controls="collapse<?= $value["cod"] ?>" class="collapsed collapseAnchor">
                        <div class="row  text-uppercase">
                            <div class="col-md-9 dis ">
                                <span class="bold" style="color: #F4CC07;">Ver ofertas realizadas en: <?= $productoData['data']['titulo'] ?></span>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="collapse<?= $value["cod"] ?>" class="collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false">
                    <div class="panel-body panel-over">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-hover">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th class="hidden-xs">Fecha</th>
                                            <th>Producto</th>
                                            <th>R.P.</th>
                                            <th>Oferta</th>
                                            <th>Ir a Lote</th>
                                            <th>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($ofertas->list(["producto = '" . $value["producto"] . "' AND usuario = '" . $_SESSION["usuarios"]["cod"] . "'"], "", "") as $oferta_) {
                                            $ofertaProducto = $ofertas->list(["rp = '" . $oferta_["rp"] . "'"], "oferta DESC", 1);
                                            $fecha = date_create($oferta_["fecha"]);
                                        ?>
                                            <tr>
                                                <td class="hidden-xs"><?= date_format($fecha, "d/m/Y h:m") ?></td>
                                                <td><?= $productoData['data']['titulo'] ?></td>
                                                <td><?= $oferta_["rp"] ?></td>
                                                <td>$<?= $oferta_["oferta"] ?></td>
                                                <td><a href="<?= URL . '/producto/' . $f->normalizar_link($productoData["data"]["titulo"]) . '/' . $productoData["data"]["cod"] ?>"> Ver Lote </a></td>
                                                <td>
                                                    <?php
                                                    if ($_SESSION['usuarios']['estado'] != 0) {
                                                        if ($oferta_['oferta'] == $ofertaProducto[0]['oferta']) {
                                                            echo "<div class='label  text-uppercase label-success'><i class='fa fa-arrow-up'></i><span class='hidden-xs'> GANANDO OFERTA </span></div>";
                                                        } else {
                                                            echo "<div class='label  text-uppercase label-danger'><i class='fa fa-arrow-down'></i><span class='hidden-xs'> OFERTA SUPERADA </span></div>";
                                                        }
                                                    } else {
                                                        echo "<div class='label  text-uppercase label-warning'><i class='fa fa-clock-o'></i> <span class='hidden-xs'>Pendiente de Verificacion </span></div>";
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>

<?php } ?>

<?php
/*
//Clases
$ofertas = new Clases\Ofertas();
$producto = new Clases\Productos();
$f = new Clases\PublicFunction();

$usuario->set("cod", $_SESSION["usuarios"]["cod"]);
$usuarioData = $usuario->view();

//$productosList = $producto->list("", "", "");
$ofertas->listGroupBy(["usuario"])

?>
<?php
foreach ($productosList as $producto_) {
    for ($i = 2; $i <= 6; $i++) {
        if (!empty($producto_['data']["variable$i"])) {
            $filter = array("usuario = '" . $usuarioData['data']['cod'] . "'");
            $ofertasProducto = $ofertas->list(["rp = '" . $producto_['data']["variable$i"] . "'", "producto = '" . $producto_['data']["cod"] . "'", "usuario = '" . $usuarioData['data']['cod'] . "'"], "", "");
            if (!empty($ofertasProducto)) {
                $fecha = explode(" ",  $ofertasProducto[0]["fecha"]);
                $fecha1 = explode("-", $fecha[0]);
                $fecha1 = $fecha1[2] . '-' . $fecha1[1] . '-' . $fecha1[0] . ' ';
                $fecha = $fecha1 . $fecha[1];
?>
                <div class="panel panel-default mt-10" style="background: lightgray">
                    <a data-toggle="collapse" href="#collapse<?= $ofertasProducto["cod"] ?>" aria-expanded="false" aria-controls="collapse<?= $ofertasProducto["cod"] ?>" class="collapsed color_a" style="width: 100%">
                        <div class="panel-heading boton-cuenta" role="tab" id="heading" style="padding: 10px;background: #8c8c8c;color: #fff;">
                            <div class="row pedido-centro text-uppercase">
                                <div class="col-md-9 dis ">
                                    <span class="negro">Oferta: <?= $ofertasProducto[0]['cod'] ?></span>
                                    <span class="negro">|| <?= $producto_['data']['titulo'] . " || R.P: " . $ofertasProducto[0]['rp'] ?></span>
                                    <span class="hidden-xs hidden-sm negro">|| Fecha <?= $fecha ?></span>
                                </div>
                                <div class="col-md-3 dis pedido-right">
                                    <?php
                                    if ($usuarioData['data']['estado'] != 0) {
                                        $ofertas->set("rp", $ofertasProducto[0]["rp"]);
                                        $mayorPrecio = $ofertas->view();
                                        if ($mayorPrecio['oferta'] == $ofertasProducto[0]['oferta']) {
                                            echo "<div class='label label-success'>ESTADO: Ganando Pre-Oferta</div>";
                                        } else {
                                            echo "<div class='label label-danger'>ESTADO: Pre-Oferta Superada</div>";
                                        }
                                    } else {
                                        echo "<div class='label label-warning'>ESTADO: Pre-Oferta Pendiente de Verificacion</div>";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
<?php
            }
        }
    }
}
?>


<div id="collapse<?= $value["cod"] ?>" class="collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
    <div class="panel-body panel-over" style="height: auto;background:#fff">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th style="width: 300px;">
                                Producto
                            </th>
                            <th style="width: 300px;">
                                R.P.
                            </th>
                            <th style="width: 300px;">
                                Oferta
                            </th>
                            <th style="width: 300px;">
                                Ir a Ofertar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 300px;"><?= $productoData['data']['titulo'] ?></td>
                            <td style="width: 300px;"><?= $value["rp"] ?></td>
                            <td style="width: 300px;">$<?= $value["oferta"] ?></td>
                            <td style="width: 300px;"><a href="<?= URL . '/producto/' . $f->normalizar_link($productoData["data"]["titulo"]) . '/' . $productoData["data"]["cod"] ?>">Ver Producto</a></td>

                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <br>
        </div>
    </div>
</div>
<?*/
?>