<?php
$config = new Clases\Config();
$contenidos = new Clases\Contenidos();

$contenidos->set("cod", "f58a11a8c3");
$contenidoData = $contenidos->view();

#Se carga la configuración de email
$emailData = $config->viewEmail();

#Se carga la configuración de contacto
$contactData = $config->viewContact();

#Se carga la configuración de redes sociales
$socialData = $config->viewSocial();
?>
<!-- start site-footer -->
<footer class="site-footer">
    <div class="pa-footer-three">
        <div class="container">
            <div class="pa-foot-box">
                <?= $contenidoData['data']['contenido'] ?>
                <div style="text-align: center; padding-top:20px">
                    <p><span style="color:#f1c40f">GPS Cabaña LL </span> Coordenadas 30º 01´ 18.89" Sur - 59º 22´ 17.02" Oeste<i class="fa fa-facebook ml-25" style="color:#EABE0E"></i><a target="_blank" href="https://www.facebook.com/delfabroagropecuaria"> /delfabroagropecuaria</a><i class="fa fa-instagram ml-20" style="color:#EABE0E;"></i><a target="_blank" href="https://www.instagram.com/lagunalimpiadelfabro/"> /lagunalimpiadelfabro</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="pa-copyright pa-copyright-two">
        <div class="container">
            <p>2020 &copy; Todos los derechos reservados <a href="https://verduleriasahara.com.ar" target="_blank"><?= TITULO ?></a>. Desarrollado por <a href="https://www.estudiorochayasoc.com.ar" target="_blank">Estudio Rocha & Asociados</a></p>
        </div>
    </div>
</footer>

<?= ($contactData['data']['whatsapp']) ? '<a href="https://api.whatsapp.com/send?phone=549' . $contactData['data']['whatsapp'] . '" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp"></i></a>' : '' ?>
<?= ($contactData['data']['messenger']) ? '<a href="https://www.messenger.com/t/' . $contactData['data']['messenger'] . '" class="messenger" target="_blank"> <i class="fa fa-facebook"></i></a>' : '' ?>

<!-- All JavaScript files
   ================================================== -->
<script src="<?= URL ?>/assets/js/jquery.min.js"></script>
<script src="<?= URL ?>/assets/js/bootstrap.min.js"></script>

<!-- Plugins for this template -->
<script src="<?= URL ?>/assets/js/jquery-plugin-collection.js"></script>
<script src="<?= URL ?>/assets/js/jquery.mCustomScrollbar.js"></script>

<!-- Custom script for this template -->
<script src="<?= URL ?>/assets/js/script.js"></script>

<!-- Default Scripts -->
<script src="<?= URL ?>/assets/js/select2.min.js"></script>
<script src="<?= URL ?>/assets/js/bootstrap-notify.min.js"></script>
<script src="<?= URL ?>/assets/js/services/services.js"></script>
<script src="<?= URL ?>/assets/js/services/cart.js"></script>