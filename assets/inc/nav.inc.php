<?php
$carrito = new Clases\Carrito();
$categorias = new Clases\Categorias();
$subcategorias = new Clases\Subcategorias();
$contenidos = new Clases\Contenidos();
$usuarios = new Clases\Usuarios();
$videos = new Clases\Videos();
$servicios = new Clases\Servicios();
$portfolio = new Clases\Portfolio();
$config = new Clases\Config();
$f = new Clases\PublicFunction();

#Se carga la sesión del usuario
$usuario = $usuarios->viewSession();

#Se cargan los productos del carrito
$carro = $carrito->return();

#Si existe la sesión y no es invitado, entonces se habilita el botón de mi cuenta en la nav
$habilitado = (isset($usuario["invitado"]) && $usuario["invitado"] == 0) ? true : false;

#Se carga la configuración de contacto
$contactData = $config->viewContact();



#Se cuentan los productos del carrito
$cont = 0;
foreach ($carro as $key => $value) {
    ($value['id'] != "Metodo-Pago" && $value['id'] != "Envio-Seleccion") ? $cont++ : null;
}

$categoriaList = $categorias->listIfHave("productos");
$serviciosList = $servicios->list("", "titulo ASC", "");
$portfolioList = $portfolio->list("", "titulo ASC", "1");
$videoList = $videos->list("", "", "1");

?>

<!-- Start header -->
<header id="header" class="site-header header-style-5">
    <!-- end lower-topbar -->
    <nav class="navigation navbar pa-main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-8 hidden-xs">
                    <a href="https://delfabro.com.ar/"><img src="<?= URL ?>/assets/images/logo.png" width="250" alt="image" class="img-fluid logo"></a>
                </div>
                <div class="col-md-3 col-8 hidden-md hidden-lg hidden-xl">
                    <a href="https://delfabro.com.ar/"><img src="<?= URL ?>/assets/images/logo.png" width="250" alt="image" class="img-fluid logo mt-15"></a>
                </div>
                <div class="col-md-9 col-4">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navigation-holder pull-right">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav text-uppercase bold">
                            <li><a class="bold" href="https://delfabro.com.ar"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                            <?php foreach ($categoriaList as $categoria) { ?>
                                <li> <a class="bold" href="<?= URL . "/productos/c/" . $f->normalizar_link($categoria["data"]["titulo"]) . "/" . $categoria["data"]["cod"] ?>"><?= $categoria["data"]["titulo"] ?></a></li>
                            <?php } ?>
                            <li><a class="bold" href="https://delfabro.com.ar/contacto"><i class="fa fa-envelope-o"></i></a></li>
                            <?php
                            if (!$habilitado) {
                            ?>
                                <li><a class="bold" href="<?= URL ?>/usuarios"><i class="fa fa-user"></i> Acceder</a></li>
                            <?php
                            } else {
                            ?>
                                <li><a class="bold" href="<?= URL ?>/sesion"><i class="fa fa-user"></i> Mi cuenta</a></li>
                                <li><a class="bold" href="<?= URL ?>/sesion?logout"><i class="fa fa-sign-out"></i> Salir</a></li>

                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div><!-- end of nav-collapse -->
        </div><!-- end of container -->
    </nav> <!-- end navigation -->
</header>
<!-- end of header -->
<?php
if ($habilitado) {
    if ($usuario['estado'] == 0) { ?>
        <div class="noValido">
            <h3 class="text-uppercase fs-16  pt-10 bold" style="color:black">Tus ofertas serán válidas luego de la verificación crediticia de tu cuenta.</h3>
        </div>
<?php }
} ?>