<?php
$f = new Clases\PublicFunction();
$carrito = new Clases\Carrito();
$usuario = new Clases\Usuarios();
$descuento = new Clases\Descuentos();
$checkout = new Clases\Checkout();

$usuarioData = $usuario->viewSession();
$descuentos = $descuento->list("", "", "");
$refreshCartDescuento = $descuento->refreshCartDescuento($carrito->return(), $usuarioData);
$carro = $carrito->return();
$i = 0;
$precio = 0;
?>
<table class="table table-striped ">
    <thead class="thead-dark ">
        <th class="text-left">Nombre</th>
        <th class="text-left hidden-sm-down">Cantidad</th>
        <th class="text-left">Precio u.</th>
        <th class="text-left">Total</th>
    </thead>
    <?php
    foreach ($carro as $key => $carroItem) {
        $precio += ($carroItem["precio"] * $carroItem["cantidad"]);
        $opciones = @implode(" - ", $carroItem["opciones"]);
        if ($carroItem["id"] == "Envio-Seleccion" || $carroItem["id"] == "Metodo-Pago") {
            $clase = "text-bold";
            $none = "hidden";
        } else {
            $clase;
            $none = "";
        }
    ?>
        <tr>
            <td>
                <b><?= mb_strtoupper($carroItem["titulo"]); ?></b>
                <?php
                if ($carroItem['descuento']['status']) {
                    foreach ($carroItem['descuento']['products'] as $itemDescuento) {
                        echo '<br> - <span class="item-titulo-descuento">' . $itemDescuento['titulo'] . ' <b class="item-monto-descuento">' . $itemDescuento['monto'] . '</b></span>';
                    }
                }
                ?>
                <br>
                <?php
                if (is_array($carroItem['opciones'])) {
                    if (isset($carroItem['opciones']['texto'])) {
                        echo $carroItem['opciones']['texto'];
                    }
                }
                ?>
                <?php if (!$carroItem['descuento']['status']) { ?>
                    <span class="amount hidden-md-up <?= $none ?>">Cantidad: <?= $carroItem["cantidad"]; ?></span>
                <?php } ?>
            </td>
            <td class="hidden-sm-down">
                <?php if (!$carroItem['descuento']['status']) { ?>
                    <span class="amount <?= $none ?>"><?= $carroItem["cantidad"]; ?></span>
                <?php } ?>
            </td>
            <td>
                <span class="amount <?= $none ?>"><?= "$" . $carroItem["precio"]; ?></span>
                <?php if (isset($carroItem["descuento"]["precio-antiguo"])) { ?>
                    <span class="<?= $none ?> descuento-precio">$<?= $carroItem["descuento"]["precio-antiguo"]; ?></span>
                <?php } ?>
            </td>
            <td>
                <?php
                if ($carroItem["precio"] != 0) {
                    echo "$" . ($carroItem["precio"] * $carroItem["cantidad"]);
                }
                ?>
            </td>
        </tr>
    <?php
        $i++;
    }
    ?>
    <tr>
        <td>
            <b>TOTAL DE LA COMPRA</b>
        </td>
        <td class="hidden-sm-down">
        </td>
        <td></td>
        <td>
            <b>$<?= number_format($carrito->totalPrice(), "2", ",", "."); ?></b>
        </td>
    </tr>
</table>